const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const sqlite3 = require("sqlite3").verbose();
const db = new sqlite3.Database(`${__dirname}/../db/webscrolls`);
var fs = require("fs");
var http = require('http');

const path = '../apps/webscrolls/media/media';

exports.doService = async jsonReq => {
  if (!validateRequest(jsonReq)) return API_CONSTANTS.API_INSUFFICIENT_PARAMS;

  try {
    const imageSave = await saveImage(jsonReq);

    return {result: imageSave};
  } catch (err) {
    console.error(err);
    return CONSTANTS.FALSE_RESULT;
  }
};

const saveImage = function(jsonReq){

    // fs.unlink(path+jsonReq.id+'.txt', function (err) {
    //     if (err) throw err;
    //     console.log('File deleted!');
    //   });

    fs.writeFile(path+jsonReq.id+'.txt', jsonReq.imageData, function(err) {
        if (err) {
           return console.error(err);
        }

        var dbData = path+jsonReq.id+'.txt';

        db.run(`UPDATE userdata 
        SET Image = "${dbData}"
        WHERE id = "${jsonReq.id}"`);
    
     });
     return true;
}



const validateRequest = jsonReq => jsonReq && jsonReq.id && jsonReq.imageData;