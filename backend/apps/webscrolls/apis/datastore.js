const sqlite3 = require('sqlite3').verbose();
const utils = require(`${__dirname}/lib/utils.js`);
// const db = require(`${API_CONSTANTS.APP_PATH}/db/websrolls`);
const nodemailer = require('nodemailer');


exports.doService = async jsonReq => {
	var db = new sqlite3.Database(__dirname+`/../db/webscrolls`);
	// LOG.info(jsonReq);
	try {
		LOG.info(jsonReq);
		var timestamp = utils.getTodayDate();
		var defaultUpdate = 0;
		var default_isArchive = 0;
		var date = new Date();
		var yearOfApplying = date.getFullYear();

		db.run(`INSERT INTO LeavesApplied(dropdown, date_from, date_to,ifhalf,no_of_days,reason,id,half, leadID, Status, Updated, CreatedAt, year, IsArchive, CreatedBy) VALUES( "${jsonReq.dropdown}","${jsonReq.from}","${jsonReq.to}","${jsonReq.ifhalf}","${jsonReq.no_of_days}","${jsonReq.reason}","${jsonReq.id}","${jsonReq.half}","${jsonReq.leadID}","${jsonReq.Status}","${defaultUpdate}","${timestamp}","${yearOfApplying}","${default_isArchive}","${jsonReq.createdBy}")`);

		const IsEmailSent = sendEmailToLeads(jsonReq);

		if(IsEmailSent) LOG.info("Emails successfully send");

		return true;
	} catch (error) {
		console.error(error);

		return {results: false, message:"Error Here!"};
	}
	
};

const sendEmailToLeads = async function(jsonReq){
	try {
		var leadIds = jsonReq.leadID;
		var numberID = leadIds.toString().match(/\d+/g).map(Number);

		for(var i=0;i<numberID.length;i++){
			
			var LeadEmail = await getLeadEmail(numberID[i]);
			var SenderName = await getSenderName(jsonReq.id);

			var sendMail = await sendEmail(LeadEmail,SenderName, jsonReq);

			if(!sendMail) LOG.info("Error Occured while sending Emails");
		}
		return true;
	} catch (error) {
		console.error(error);

		return {results: false, message:"Error Here!"};
	}
}

const getSenderName = function(id){
	var db = new sqlite3.Database(__dirname+`/../db/webscrolls`);

	try {
		return new Promise((resolve,reject)=>{
			db.get(`SELECT * FROM userdata where id=? `,id, (err, row) => {
				if(typeof(row) == "undefined" || err)	return resolve(false);
				
				return resolve(row.emp_name);
			  });
		})
	} catch (error) {
		console.error(error);
	}
}

const getLeadEmail = function(id){
	var db = new sqlite3.Database(__dirname+`/../db/webscrolls`);

	try {
		return new Promise((resolve, reject)=>{
			db.get(`SELECT * FROM userdata where id=? `,id, (err, row) => {
				if(typeof(row) == "undefined" || err)	return resolve(false);
				LOG.info(row);
                return resolve(row.email);
              });
		});
	} catch (error) {
		console.error(error);

		LOG.info("In this box");
	}
	
}

const sendEmail = function(email,name,req,res){

    try {
	  var name = name;
      var from = `arvind.chaudhary@deeplogictech.com`;
      var message = "Leave Request from "+name+ 
	  "\nFrom: "+req.from+
	  "\nTo: "+req.to+
	  "\nLeave Type: "+req.ifhalf+
	  "\nReason: "+req.reason;
      var to = email;
      var smtpTransport = nodemailer.createTransport("smtps://arvind.chaudhary9778@gmail.com:"+encodeURIComponent('arvind123arvind') + "@smtp.gmail.com:465"); 

      var mailOptions = {
          from: from,
          to: to, 
          subject: 'Leave Request from '+ name,
          text: message
      }
      // un-comment this to send email

      smtpTransport.sendMail(mailOptions, function(error, response){
          if(error){
              console.log(error);
          }else{
              res.redirect('/');
          }
      });
    } catch (error) {
        console.error(error);
    }
  return true;
}

// let validateRequest = jsonReq => (jsonReq && jsonReq.u_username && jsonReq.u_password);