const sqlite3 = require('sqlite3').verbose();
const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const crypto = require("crypto");
const utils = require(`${__dirname}/lib/utils.js`);

// const db = require(`${API_CONSTANTS.APP_PATH}/db/websrolls`);
const db = new sqlite3.Database(`${__dirname}/../db/webscrolls`);


exports.doService = async jsonReq => {
	
    if (!validateRequest(jsonReq)) return API_CONSTANTS.API_INSUFFICIENT_PARAMS;
	try {
		// LOG.info(jsonReq.u_username.trim());
		const response = jsonReq.u_username;

		const password = await loginUser(jsonReq);
		if(!password) return API_CONSTANTS.API_RESPONSE_LOGIN_FAIL;
		const login = await check_pass(password, jsonReq);
		
		if (!login) return API_CONSTANTS.API_RESPONSE_LOGIN_FAIL;
		
		return { result: true, results: response, message: "Login Successful" };
	} catch (error) {
		return false;
	}	
};

const loginUser = jsonReq =>{
    return new Promise((resolve, reject) => {
			try {
				db.all(`SELECT * FROM UserLoginInfo where u_username=? `,jsonReq.u_username.trim(), (err, row) => {
					if(typeof(row[0]) == "undefined" || err)	return resolve(false);
	
					return resolve(row[0]);
				  });
			} catch (error) {
				return false;
			}
		});
};

const check_pass = function(password, jsonReq){
	try {
		// if(password === jsonReq.u_password.trim())
		// 	return true;
		// return false;

		const u_password = sha512(jsonReq.u_password, password.u_passwordSecret);
		LOG.info(u_password);
		if(u_password === password.u_password)		
			return true;

		return false;

	} catch (error) {
		console.error(error);
	}
}

// Hashing algorithm: SHA512
const sha512 = (password, salt) => crypto.createHmac("sha512", salt).update(password).digest("hex");

const validateRequest = jsonReq => (jsonReq && jsonReq.u_username && jsonReq.u_password);