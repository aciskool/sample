const sqlite3 = require('sqlite3').verbose();
const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const crypto = require("crypto");

// const db = require(`${API_CONSTANTS.APP_PATH}/db/websrolls`);
const db = new sqlite3.Database(`${__dirname}/../db/webscrolls`);


exports.doService = async jsonReq => {
	
    if (!validateRequest(jsonReq)) return API_CONSTANTS.API_INSUFFICIENT_PARAMS;
	try {

        var subID = await getSubID(jsonReq);
        if(!subID) return API_CONSTANTS.API_RESPONSE_LOGIN_FAIL;
        
        var numberID = subID.toString().match(/\d+/g).map(Number);
        var returnData = {};
        var returnLength;
        for(var i=0,k=0;i<numberID.length;i++){
            var name = await getName(numberID[i]);
            var leaveData = await getLeaveData(numberID[i]);
            if(!leaveData)  continue;
            for(var j=0;j<leaveData.length;j++){
                returnData[k] = {name: name, leavetype: leaveData[j].dropdown, from: leaveData[j].date_from, to: leaveData[j].date_to, noDays: leaveData[j].no_of_days, status: leaveData[j].Status, subID: leaveData[j].id, form_ID: leaveData[j].form_ID};
                k++;
            }
            returnLength = k;
        }
        return { result: true, message: returnData , returnLength };
	} catch (error) {
		return false;
	}	
};

const getSubID = jsonReq =>{
    return new Promise((resolve, reject) => {
			try {
				db.get(`SELECT * FROM userdata where id=? `,jsonReq.id, (err, row) => {
					if(typeof(row) == "undefined" || err)	return resolve(false);
					return resolve(row.subID);
				  });
			} catch (error) {
				return false;
			}
		});
};

const getName = id => {
    return new Promise((resolve, reject) => {
        try {
            db.get(`SELECT * FROM userdata where id=? `,id, (err, row) => {
                if(typeof(row) == "undefined" || err)	return resolve(false);

                return resolve(row.emp_name);
              });
        } catch (error) {
            return false;
        }
    });
};

const getLeaveData = id => {
    return new Promise((resolve, reject) => {
        try {
            db.all(`SELECT * FROM LeavesApplied where id=? AND IsArchive=0`,id, (err, row) => {
                if(typeof(row[0]) == "undefined" || err)	return resolve(false);
                return resolve(row);
              });
        } catch (error) {
            return false;
        }
    });
};


const validateRequest = jsonReq => (jsonReq && jsonReq.id);