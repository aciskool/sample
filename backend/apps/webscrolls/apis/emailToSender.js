const sqlite3 = require('sqlite3').verbose();
const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const crypto = require("crypto");

// const db = require(`${API_CONSTANTS.APP_PATH}/db/websrolls`);

var db = new sqlite3.Database(__dirname+`/../db/webscrolls`);
const nodemailer = require('nodemailer');

exports.doService = async jsonReq => {
    
    let sendEmail = await sendEmailTOSender(jsonReq);

    if(!sendEmail) return {results: false, message:"There was an error in sending mails."}

    return {results:true, message:"Email were successfully send!"};
};

const sendEmailTOSender = async function(jsonReq){
    try {
            var leadName = await getLeadName(jsonReq);

            var LeaveInfo = await getLeaveInfo(jsonReq);

            var email = await getApplierEmail(jsonReq);

            var sendEmailCheck = await sendEamilToApplier(leadName, LeaveInfo, email, jsonReq);
            
            if(!sendEmailCheck) return false;

            return true;
    } catch (error) {
        console.error(error);
    }
    
}

const getLeadName = function(jsonReq){
    return new Promise((resolve,reject)=>{
        db.get(`SELECT * FROM userdata WHERE id=?`,jsonReq.leadID, (err, row)=>{
            if(err) return reject(err);

            return resolve(row.emp_name);
        })
    });
}

const getLeaveInfo = function(jsonReq){
    return new Promise((resolve,reject)=>{
        db.get(`SELECT * FROM LeavesApplied WHERE form_ID=?`,jsonReq.LeaveID, (err, row)=>{
            if(err) return reject(err);

            return resolve(row);
        })
    });
}

const getApplierEmail = function(jsonReq){
    return new Promise((resolve,reject)=>{
        db.get(`SELECT * FROM userdata WHERE id=?`,jsonReq.EmpID, (err, row)=>{
            if(err) return reject(err);

            return resolve(row.email);
        })
    });
}

const sendEamilToApplier = function(leadName, LeaveInfo, email, jsonReq){

    try {
        var local_status = (jsonReq.response)?"Accepted":"Rejected";
        var name = leadName;
        var from = `arvind.chaudhary@deeplogictech.com`;
        var message = "Leave Request: "+local_status+ 
        "\nFrom: "+LeaveInfo.date_from+
        "\nTo: "+LeaveInfo.date_to+
        "\nLead Name: "+name+
        "\nReason: "+jsonReq.comment+
        "\nOverall Leave Status: "+LeaveInfo.Status;
        var to = email;
        var smtpTransport = nodemailer.createTransport("smtps://arvind.chaudhary9778@gmail.com:"+encodeURIComponent('arvind123arvind') + "@smtp.gmail.com:465"); 
  
        var mailOptions = {
            from: from,
            to: to, 
            subject: 'Leave Request '+local_status+' by '+leadName ,
            text: message
        }
        // un-comment this to send email
  
        smtpTransport.sendMail(mailOptions, function(error, response){
            if(error){
                console.log(error);
            }else{
                res.redirect('/');
            }
        });
      } catch (error) {
          console.error(error);
      }
    return true;

}