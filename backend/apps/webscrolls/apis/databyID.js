const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);

const sqlite3 = require("sqlite3").verbose();
const db = new sqlite3.Database(`${__dirname}/../db/webscrolls`);

exports.doService = async jsonReq => {
  try {
    const id = jsonReq.id;

    const userData = await getUser(id);
    if (!userData) return { result: false, message: "User data not found" };

    return userData;
  } catch (err) {
    console.error(err);
    return CONSTANTS.FALSE_RESULT;
  }
};

const getUser = userId => {
    return new Promise((resolve, reject) => {
      db.get(`SELECT * FROM userdata where id=?`,userId, (err, row) => {
        if (err) return reject(err);
        return resolve(row);
      });
    });
  };