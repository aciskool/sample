const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const sqlite3 = require("sqlite3").verbose();
const db = new sqlite3.Database(`${__dirname}/../db/webscrolls`);

exports.doService = async jsonReq => {
  try {
    const id = jsonReq.id;
    let LeadData = await getLeadData(id);

    if(!LeadData) return {result: false , message:"Error in finding Lead Data"};

    return {result:true , returndata: LeadData};
  } catch (err) {
    console.error(err);
  }
};

const getLeadData = function(id){
    return new Promise((resolve,reject) =>{
        try {
            db.get(`SELECT * FROM userdata WHERE id=?`,id,(err,row)=>{
                if(typeof(row) == undefined || err)  return resolve(false);
    
                return resolve(row.LeadID);
            })
        } catch (error) {
            return false;
        }
    })
}