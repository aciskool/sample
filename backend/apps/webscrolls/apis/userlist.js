const sqlite3 = require('sqlite3').verbose();
const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const crypto = require("crypto");

// const db = require(`${API_CONSTANTS.APP_PATH}/db/websrolls`);
const db = new sqlite3.Database(`${__dirname}/../db/webscrolls`);


exports.doService = async function() {
	
	try {
        var UserData = await getUserData();
		return { result: true, UserData: UserData};
	} catch (error) {
		return false;
	}	
};

const getUserData = function(){
    return new Promise((resolve,reject) =>{
        try {
            db.all(`SELECT * FROM userdata`, (err,row) =>{
                if(typeof(row[0]) == "undefined" || err)	return resolve(false);
                return resolve(row);
            })
        } catch (error) 
        {
            return false;
        }
    })
}

