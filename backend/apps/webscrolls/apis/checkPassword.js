const sqlite3 = require('sqlite3').verbose();
const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const crypto = require("crypto");

const db = new sqlite3.Database(`${__dirname}/../db/webscrolls`);


exports.doService = async jsonReq => {
	
    if (!validateRequest(jsonReq)) return API_CONSTANTS.API_INSUFFICIENT_PARAMS;
	try {
        var check = await checkPassword(jsonReq);
        
        if(!check) return {result: false};
        
        return { result: true};
        
	} catch (error) {
		return false;
	}	
};

const checkPassword = jsonReq =>{
    return new Promise((resolve, reject) => {
        try {
            db.get(`SELECT * FROM UserLoginInfo where id=? `,jsonReq.id, (err, row) => {
                if(typeof(row) == "undefined" || err)	return resolve(false);

                const userPassword = sha512(jsonReq.password , row.u_passwordSecret);
                if(userPassword === row.u_password)
                    return resolve(true);

                return resolve(false);
              });
        } catch (error) {
            return false;
        }
    });
}

const sha512 = (password, salt) => crypto.createHmac("sha512", salt).update(password).digest("hex");

const validateRequest = jsonReq => (jsonReq && jsonReq.id && jsonReq.password);