const fs = require("fs");
const fsPromises = fs.promises;
const API_CONSTANTS = require(`${CONSTANTS.APPROOTDIR}/yom/apis/lib/constants`);
const db = require(`${API_CONSTANTS.LIB_PATH}/db`);
const utils = require(`${API_CONSTANTS.LIB_PATH}/utils`);

exports.doService = async jsonReq => {
// Validate API
if (!validateRequest(jsonReq)) return API_CONSTANTS.API_INSUFFICIENT_PARAMS;

let connection;
try {
connection = await db.beginTransaction();
if (!connection) return API_CONSTANTS.API_RESPONSE_DB_NOT_CONNECTED;

const writeFileStatus = await writeFile(jsonReq, connection);
if (!writeFileStatus) {
await db.rollBack(connection);
return { result: false, message: "File Not saved" };
}
if (await db.commit(connection)) return { result: true, message: "File uploaded." }
await db.rollBack(connection);
return { result: false, message: "File Not saved" };
} catch (error) {
console.error(error);
await db.rollBack(connection);
return API_CONSTANTS.API_RESPONSE_SERVER_ERROR;
}
};

const writeFile = async (jsonReq, connection) => {
try {
let docType;
const fileData = jsonReq.filesDetails.fileData;
let data;
switch (jsonReq.filesDetails.file_type) {
// for png image "file_type":"image/png"
case 'image/png':
docType = "r/";
data = fileData.replace(/^data:image\/\w+;base64,/, "");
break;
case 'image/jpeg':
docType = "r/";
// strip off the data: url prefix to get just the base64-encoded bytes
data = fileData.replace(/^data:image\/\w+;base64,/, "");
break;
default:
}

const userId = jsonReq.userId;
const tempName = jsonReq.filesDetails.d_name;
let uploadePath;
if (jsonReq.filesDetails.e_event)
uploadePath = `home/deep/yeponemore/yom/backend/apps/yom/files/events-icons/${docType}${jsonReq.filesDetails.e_event}/${userId}/${utils.getTimestamp()}/${tempName}`;
else
uploadePath = `home/deep/yeponemore/yom/backend/apps/yom/files/profile-images/${userId}/${utils.getTimestamp()}/${tempName}`;

jsonReq.filesDetails['d_path'] = uploadePath;

if (!data) return

let fileBuffer = Buffer.from(data, 'base64');

const uploadStatus = await createFile(uploadePath, fileBuffer);
if (uploadStatus === false) return false;

if (!data) return false;
if (!await addDocumentRecord(jsonReq, connection)) return false;
return true;

} catch (error) {
throw error;
}
};

const createDir = async (dirPath) => {
if(fs.mkdirSync(dirPath, { recursive: true }))
return true;
else
return false;
}

const createFile = async (uploadePath, fileBuffer) => {
let creatDirectory = await createDir(uploadePath);
if (creatDirectory) {
fs.writeFile(uploadePath, fileBuffer, (error) => {
if (error) {
LOG.info(error);
return false;
}
else
return true;
})
}
else return false;
};


const addDocumentRecord = async (jsonReq, connection) => {
try {
const timestamp = utils.getTimestamp();

// Save changeLogs
const addChangeLog = {
"cl_userId": jsonReq.userId,
"cl_action": 1,
"cl_module": "Document",
"cl_entity": "New Document",
"cl_createdAt": timestamp
}
if (!await db.consecutiveInsert(connection, 'changeLogs', addChangeLog)) return false;
return jsonReq.userId;
} catch (error) {
throw error;
}
};

const validateRequest = jsonReq => (jsonReq && jsonReq.userId && jsonReq.filesDetails.d_name);