const sqlite3 = require('sqlite3').verbose();

var db = new sqlite3.Database(__dirname+`/../db/webscrolls`);


exports.doService = async jsonReq => {
    
    var returnData = await getData();
    return returnData;
};

let getData = async function(){
    return new Promise((resolve, reject) => {
        db.all(`SELECT * FROM LeavesApplied WHERE Status="Pending" AND IsArchive=0`, (err, row) => {
          if (err) return reject(err);
          return resolve(row);
        });
      });
}