const sqlite3 = require('sqlite3').verbose();
const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const crypto = require("crypto");
const utils = require(`${__dirname}/lib/utils.js`);

const nodemailer = require('nodemailer');


exports.doService = async jsonReq => {
    if(!validateRequest(jsonReq))   return {results:false};

    let tempPassword = await updateLoginInfo(jsonReq);

    let checkStatus = await contact(jsonReq, tempPassword);
    // LOG.info(checkStatus);
    if(!checkStatus) return "false;"
    
    return {result: true , message : "Sent the email for password recovery"};
}

const contact = function(req,password,res){

    try {
      var name = req.name;
      var from = `arvind.chaudhary@deeplogictech.com`;
      var tempPassword = password;
      var message = "New Temporary Password is: "+tempPassword;
      var to = req.email;
      var smtpTransport = nodemailer.createTransport("smtps://arvind.chaudhary9778@gmail.com:"+encodeURIComponent('arvind123arvind') + "@smtp.gmail.com:465"); 

      var mailOptions = {
          from: from,
          to: to, 
          subject: 'Password Recovery',
          text: message
      }
      // un-comment this to send email

      smtpTransport.sendMail(mailOptions, function(error, response){
          if(error){
              console.log(error);
          }else{
              res.redirect('/');
          }
      });
    } catch (error) {
        console.error(error);
    }
  return true;
}

const updateLoginInfo = async function(jsonReq){

  var db = new sqlite3.Database(__dirname+`/../db/webscrolls`);

  try {
    return new Promise((resolve,reject) =>{
    db.get(`SELECT * FROM UserLoginInfo where u_username=? `,jsonReq.name.trim(), (err, row) => {
      if(typeof(row) == "undefined" || err)	return resolve(false);

      var u_passwordSecret = row.u_passwordSecret;
      var tempPassword = utils.randomCharacters();
      var timestamp = utils.getTodayDate();
      var u_password = sha512(tempPassword, u_passwordSecret);

      db.run(`UPDATE UserLoginInfo 
        SET u_password = "${u_password}",
            CreatedAt = "${timestamp}"

        WHERE u_username = "${jsonReq.name.trim()}"`);

        return resolve(tempPassword);
      })
    });  

  } catch (error) {
      console.error(error);
  }
}

const sha512 = (password, salt) => crypto.createHmac("sha512", salt).update(password).digest("hex");

const validateRequest = jsonReq => (jsonReq && jsonReq.name && jsonReq.email);