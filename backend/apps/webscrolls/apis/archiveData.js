const sqlite3 = require('sqlite3').verbose();
const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const crypto = require("crypto");

// const db = require(`${API_CONSTANTS.APP_PATH}/db/websrolls`);
const db = new sqlite3.Database(`${__dirname}/../db/webscrolls`);


exports.doService = async jsonReq => {
	
    if (!validateRequest(jsonReq)) return API_CONSTANTS.API_INSUFFICIENT_PARAMS;

	try {


		var checkArchive = await checkYearinArchive(jsonReq);

		if(checkArchive.length == 0){
			var changeIsArchive = await changeIsArchiveValue(jsonReq);

			if(!changeIsArchive)   LOG.info("IsArchive Option wasnot changed Properly");

			var leaveData = await getLeaveData();

			if(!leaveData) return {result: false, message:"Unable to retreive Leave Data"};

			var updateArchives = await updateDatainArchives(leaveData, jsonReq);

			if(!updateArchives) return {result:false , message:"Cannot update data in Archives."}

			var setDefault = await setLeavesDefault();

			if(!setDefault) LOG.info("Leaves are not changed to default");

			var addYear = await addYearinArchive(jsonReq);

			if(!addYear)  LOG.info("Year not added in Archived Year Table");

			return {result: true, message:"Year has been archived"};
		}

		else return {result: false , message:"Year already archived"};
        
	} catch (error) {
		return false;
	}	
};

const checkYearinArchive = function(jsonReq){
	return new Promise((resolve, reject) => {
		try {
			db.all(`SELECT * FROM ArchivedYear WHERE year=?`,jsonReq.year,(err,row)=>{
				if(typeof(row[0])==undefined || err)	return resolve(false);
		
				return resolve(row);
			});
		} catch (error) {
			console.error(error);
			return false;
		}
	});
}

const changeIsArchiveValue = function(jsonReq){
	try {
		db.all(`UPDATE LeavesApplied 
			SET IsArchive = 1
			
			WHERE year="${jsonReq.year}" AND IsArchive = 0`);

		db.all(`UPDATE LeaveStatus 
		SET IsArchive = 1
		
		WHERE year="${jsonReq.year}" AND IsArchive = 0`);

		return true;
	} catch (error) {
		return false;
	}
}

const getLeaveData = function(){
    return new Promise((resolve, reject) => {
		try {
			db.all(`SELECT * FROM LeaveProvided`, (err, row) => {
				if(typeof(row[0]) == "undefined" || err)	return resolve(false);

				return resolve(row);
			  });
		} catch (error) {
			return false;
		}
	});
}

const updateDatainArchives = function(leaveData, jsonReq){
	try {
		for(var i=0;i<leaveData.length;i++){
			db.run(`INSERT INTO ArchiveData(id, pl_left, cl_left, Complementary, UsedComp, year) VALUES ("${leaveData[i].id}","${leaveData[i].AvailablePL}","${leaveData[i].AvailableCL}","${leaveData[i].Complementary}","${leaveData[i].UsedComp}","${jsonReq.year}")`);
		}

		return true;
	} catch (error) {
		console.error(error);
		return false;
	}
	
}

const setLeavesDefault = function(){
	try {
		db.all(`UPDATE LeaveProvided 
				SET AvailableCL = 3,
					AvailablePL = 12,
					Complementary = 0,
					UsedComp = 0
		`);

		return true;
	} catch (error) {
		return false;
	}
}

const addYearinArchive = function(jsonReq){
	try {
		db.run(`INSERT INTO ArchivedYear (year) VALUES("${jsonReq.year}")`);

		return true;
	} catch (error) {
		return false;
	}
}

const validateRequest = jsonReq => (jsonReq && jsonReq.year);

