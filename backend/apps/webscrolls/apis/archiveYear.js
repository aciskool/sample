const sqlite3 = require('sqlite3').verbose();
const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const crypto = require("crypto");

// const db = require(`${API_CONSTANTS.APP_PATH}/db/websrolls`);
const db = new sqlite3.Database(`${__dirname}/../db/webscrolls`);


exports.doService = async jsonReq=> {
	
	try {
        var YearData = await getYear();

        if(!YearData)   return {result: false, message:"Unable to get Year Data"};

        return {result:true , message: YearData};
        
	} catch (error) {
		return false;
	}	
};

const getYear = function(){
    return new Promise((resolve,reject)=>{
        db.all(`SELECT * FROM ArchivedYear`,(err,row)=>{
            if(typeof(row)==undefined || err)   return resolve(false);

            return resolve(row);
        })
    });
}
