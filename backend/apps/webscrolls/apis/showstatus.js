const sqlite3 = require('sqlite3').verbose();
const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const crypto = require("crypto");

const db = new sqlite3.Database(`${__dirname}/../db/webscrolls`);


exports.doService = async jsonReq => {
    if(!validateRequest(jsonReq)) return API_CONSTANTS.API_INSUFFICIENT_PARAMS;
    try 
    {
        let leadIds = await getLeadID(jsonReq);

        var numberID = leadIds.toString().match(/\d+/g).map(Number);
        
        var finalResponse = {};
        for(var i=0; i<numberID.length; i++)
        {
            var leadName = await getLeadName(numberID[i]);
            var response = await getLeadResponse(numberID[i],jsonReq);
            LOG.info(response);
            if(!response) {finalResponse[i] = {name: leadName.emp_name, action: "Pending", reason: "none", Leads: numberID.length};}
            
            else finalResponse[i] = {name: leadName.emp_name, action: (response.Response)?"Accepted":"Rejected", reason: response.Comment, Leads: numberID.length};
        }
    
        return{
            result: true,
            message: finalResponse
        };
    } 
    catch (error) {
        return false;
    }

};	

const getLeadID = jsonReq =>{
    return new Promise((resolve, reject) => {
        try {
            db.get(`SELECT * FROM userdata where id=? `,jsonReq.id, (err, row) => {
                if(typeof(row) == "undefined" || err)	return resolve(false);
                return resolve(row.LeadID);
              });
        } catch (error) {
            return false;
        }
    });
};

const getLeadResponse = (id,jsonReq) => {
    return new Promise((resolve, reject) => {
        try {
            db.get(`SELECT * FROM LeaveStatus where LeadID=? AND LeaveID=? AND IsArchive=0`,id,jsonReq.formID, (err, row) => {
                if(typeof(row) == "undefined" || err)	return resolve(false);

                return resolve(row);
              });
        } catch (error) {
            return false;
        }
    });
}

const getLeadName = id => {
    return new Promise((resolve, reject) => {
        try {
            db.get(`SELECT * FROM userdata where id=?`,id, (err, row) => {
                if(typeof(row) == "undefined" || err)	return resolve(false);

                return resolve(row);
              });
        } catch (error) {
            return false;
        }
    });    
}

const validateRequest = jsonReq => (jsonReq && jsonReq.id && jsonReq.formID);