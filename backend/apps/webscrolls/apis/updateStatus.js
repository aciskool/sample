const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const sqlite3 = require("sqlite3").verbose();
const db = new sqlite3.Database(`${__dirname}/../db/webscrolls`);

exports.doService = async jsonReq => {
    db.all(`SELECT * from LeaveStatus where LeaveID=?`,jsonReq.id, (err,row) =>{
        if(err) console.error(err);

        var returnValue = "Accepted";

        if(row.length == jsonReq.numberLeads){
            for(var i=0;i<row.length;i++)
            {
                if(row[i].Response == 0){
                    returnValue = "Rejected";
                    break;
                }
            }
        }
        else{
            returnValue = "Pending";
        }
        db.run(`UPDATE LeavesApplied 
        SET Status = "${returnValue}"
        WHERE form_id = "${jsonReq.id}"`);
    });
    // db.close();
    return true;
};
