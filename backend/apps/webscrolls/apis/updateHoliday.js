const sqlite3 = require('sqlite3').verbose();
const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const crypto = require("crypto");

const db = new sqlite3.Database(`${__dirname}/../db/webscrolls`);


exports.doService = async jsonReq => {
    try 
    {
        if(!validateRequest(jsonReq))   return false;
        var hoildayData = await storeHolidayData(jsonReq);
        if(!hoildayData)  return {result: false , message: "Unable to add Holiday Data"};

        return {result: true, message:"Holiday Added"};
    } 
    catch (error) {
        return false;
    }
};	

const storeHolidayData = function(jsonReq){
    try {
        db.run(`INSERT INTO holiday (holiday,date,day, createdBy) VALUES("${jsonReq.name}","${jsonReq.date}","${jsonReq.day}","${jsonReq.createdBy}")`);

        return true;
    } catch (error) {
        return false;
    }
}

const validateRequest = jsonReq => (jsonReq && jsonReq.name && jsonReq.date && jsonReq.day);