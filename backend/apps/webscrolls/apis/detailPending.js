const sqlite3 = require('sqlite3').verbose();

var db = new sqlite3.Database(__dirname+`/../db/webscrolls`);


exports.doService = async jsonReq => {
	
    const id = jsonReq.id;  
    var returnData = getData(id);
    LOG.info(returnData);
    return returnData;
};

let getData = async function(id){
    return new Promise((resolve, reject) => {
        db.all(`SELECT * FROM LeavesApplied where id=? AND Status="Pending" AND IsArchive=0`,id, (err, row) => {
          if (err) return reject(err);
          return resolve(row);
        });
      });
}