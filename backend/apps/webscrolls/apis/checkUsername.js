const sqlite3 = require('sqlite3').verbose();
const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const crypto = require("crypto");

// const db = require(`${API_CONSTANTS.APP_PATH}/db/websrolls`);
const db = new sqlite3.Database(`${__dirname}/../db/webscrolls`);


exports.doService = async jsonReq => {
	
    if (!validateRequest(jsonReq)) return API_CONSTANTS.API_INSUFFICIENT_PARAMS;
	try {

		var check = await checkAvailable(jsonReq);
		if(!check) return {result: false};
		return { result: true};
	} catch (error) {
		return false;
	}	
};

const checkAvailable = jsonReq =>{
    return new Promise((resolve, reject) => {
        try {
            db.get(`SELECT * FROM UserLoginInfo where u_username=? `,jsonReq.username.trim(), (err, row) => {
                if(typeof(row) == "undefined" || err)	return resolve(false);

                return resolve(true);
              });
        } catch (error) {
            return false;
        }
    });
}

const validateRequest = jsonReq => (jsonReq && jsonReq.username);