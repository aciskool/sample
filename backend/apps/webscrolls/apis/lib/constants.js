/* 
 * (C) 2015 TekMonks. All rights reserved.
 * License: MIT - see enclosed LICENSE file.
 */

const path = require("path");

APP_ROOT = `${path.resolve(`${__dirname}/../../`)}`;
exports.CMS_ROOT = `${path.resolve(`${__dirname}/../../../../../frontend/apps/webscrolls/articles`)}`;

exports.LOGIN_ROOT = `${path.resolve(`${__dirname}/../../../../../frontend/apps/webscrolls/js/loginform.mjs`)}`;

/* Constants for the FS Login subsystem */
exports.SALT_PW = "$2a$10$VFyiln/PpFyZc.ABoi4ppf";
exports.APP_DB = `${APP_ROOT}/db/webscrolls.db`;

exports.API_INSUFFICIENT_PARAMS = { result: false, message: "Insufficient Parameters" }
exports.API_RESPONSE_LOGIN_FAIL = {result: false, message: "Incorrect login details"}
exports.API_RESPONSE_CHANGE_PASS_FAIL = {result: false, message: "Username and Password doesnot match"}

// Simple API Response for success or failure
exports.API_RESPONSE_TRUE = { result: true };
exports.API_RESPONSE_FALSE = { result: false };


