/* 
 * (C) 2020 TekMonks. All rights reserved.
 * License: GPL2 - see enclosed LICENSE file.
 */
/** 
 * Generate random RFC-compliant UUIDs in JavaScript
 * source: https://github.com/kelektiv/node-uuid
 */
module.exports.uniqid = () => require(__dirname + "/../../3p/node_modules/uuid/v4")();

/** Generate random number using current timestamp */
module.exports.randomNumber = () => Math.floor(Math.random() * Date.now() / 1000);

/** Generate random characters can be used as strong password */
module.exports.randomCharacters = (length = 20, wishlist = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz~!@-#$") => Array(length).fill('').map(() => wishlist[Math.floor(Math.random() * wishlist.length)]).join('');

/** Enable or disable info logs from here */
module.exports.captureInfoLog = (toLog, disableLog = false) => (!disableLog) ? LOG.info(toLog) : undefined;

/** Get Unixtime stamp */
module.exports.getTimestamp = (date) => (date) ? new Date(date).getTime() : new Date().getTime();

/** String passed string for new line or additional spaces */
module.exports.stripString = (inputString) => (inputString) ? inputString.replace(/(\r\n|\n|\r)/gm, "").replace(/\s+/g, ' ') : "";

/** Returns Array of unique values within the provided Array */
module.exports.getUniqueValues = (inputArray) => Object.values(inputArray).filter((value, index, self) => (self.indexOf(value) === index));

/** Get Current Unix timestamp without milliseconds */
module.exports.getCurrentUnixTimestamp = () => (new Date().getTime() / 1000).toString().split('.')[0];

module.exports.getTodayDate = function(){
    let date = new Date();
    var dat = date.getDate();
    var month = date.getMonth();
    var year = date.getFullYear();
    let monthText = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec"];
    //
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    // return strTime;
    //
    let monthInText = monthText[month];
    var compDate = monthInText + " " + dat + ","+" "+year+" | "+strTime;
    return compDate;
}