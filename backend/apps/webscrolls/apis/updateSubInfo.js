const sqlite3 = require('sqlite3').verbose();
const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const crypto = require("crypto");
const utils = require(`${__dirname}/lib/utils.js`);

var db = new sqlite3.Database(__dirname+`/../db/webscrolls`);


exports.doService = async jsonReq => {

    try {
        LOG.info(jsonReq);

        var subID = await getSubID(jsonReq);
        LOG.info("ID is: "+ subID);
        if(!subID)  return {results: false , message:"Cannot find Sub ID"};

        for(var i=0;i<jsonReq.LeadInfo.length;i++){
            var updateSubID = await updateLeadData(subID, jsonReq.LeadInfo[i]);

            if(!updateSubID)  LOG.info("SUB ID were not added!");
        }

        return {results:true , message:"User Added"};

    } catch (error) {
            return false;
    }
};

let getSubID = async function(jsonReq){
    // var db = new sqlite3.Database(__dirname+`/../db/webscrolls`);
    return new Promise((resolve, reject) => {
        try {
            db.get(`SELECT * FROM userdata where u_username=? `,jsonReq.username.trim(), (err, row) => {
                if(typeof(row) == "undefined" || err)	{LOG.info(err); return resolve(false); }

                return resolve(row.id);
              });
        } catch (error) {
            return false;
        }
    });
}

const updateLeadData = function(addID, leadID){
    // var db = new sqlite3.Database(__dirname+`/../db/webscrolls`);
    // return new Promise((resolve, reject) => {
        try {
             db.get(`SELECT * FROM userdata WHERE id=?`,leadID,(err,row)=>{
                if(typeof(row) == "undefined" || err)	return resolve(false);

                var storedSubID = row.subID;
                LOG.info("SUBID IS:"+storedSubID);
                if(storedSubID == "" || storedSubID == null)     storedSubID+=addID;

                else{
                    var numberID = storedSubID.toString().match(/\d+/g).map(Number);
                    for(var i=0;i<numberID.length;i++){
                        if(numberID[i] == addID)
                            return true;
                    }

                    storedSubID+=",";
                    storedSubID+=addID;
                }
                db.run(`UPDATE userdata 
                    SET subID = "${storedSubID}"
                    WHERE id = "${leadID}"`);
                // return resolve(subID);
             })   
            //  db.close();
             return true; 
        } catch (error) {
            return false;
        }
}

