const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const sqlite3 = require("sqlite3").verbose();
const db = new sqlite3.Database(`${__dirname}/../db/webscrolls`);
var fs = require("fs");
var http = require('http');

const path = '../apps/webscrolls/media/media';

exports.doService = async jsonReq => {
  if (!validateRequest(jsonReq)) return API_CONSTANTS.API_INSUFFICIENT_PARAMS;

  try {
    let imageSave = await getImageData(jsonReq);

    if(imageSave == false) return {result: false};

    return {result: imageSave.toString() };

  } catch (err) {
        console.error(err);
        return CONSTANTS.FALSE_RESULT;
  }
};

const getImageData = function(jsonReq){
    return new Promise((resolve, reject)=>{
        fs.readFile(path+jsonReq.id+'.txt', (err, data) => {
            if (err) {
              console.error(err)
              return resolve(false);
            }
            // LOG.info(data);
            return resolve(data);
          })
    });
}



const validateRequest = jsonReq => jsonReq && jsonReq.id ;