const sqlite3 = require('sqlite3').verbose();
const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const crypto = require("crypto");

// const db = require(`${API_CONSTANTS.APP_PATH}/db/websrolls`);
const db = new sqlite3.Database(`${__dirname}/../db/webscrolls`);


exports.doService = async jsonReq => {
	
    if (!validateRequest(jsonReq)) return API_CONSTANTS.API_INSUFFICIENT_PARAMS;
	try {

        var updateLeave = updateCompLeave(jsonReq.id,jsonReq.leave);

        if(!updateLeave) return {result: false , message:"Complementary Leaves not updated"};

        return {result: true, message:"Complementary Leaves Updated"};

	} catch (error) {
		return false;
	}	
};

const updateCompLeave = function(id, leave){
    try {
        db.get(`SELECT * FROM LeaveProvided WHERE id=?`,id,(err,row)=>{
            if(typeof(row)==undefined || err)  return false;

            var compLeave = row.Complementary;
            if(compLeave==null || compLeave==0 || compLeave == ""){
                compLeave = leave;
            }
            else{
                compLeave+= parseFloat(leave);
            }
            db.run(`UPDATE LeaveProvided
                    SET Complementary = "${compLeave}"    

                    WHERE id="${id}"`);
        });
    return true;
    } catch (error) {
        
    }
}

const validateRequest = jsonReq => (jsonReq && jsonReq.id && jsonReq.leave);