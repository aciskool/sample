const sqlite3 = require('sqlite3').verbose();
const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const crypto = require("crypto");

// const db = require(`${API_CONSTANTS.APP_PATH}/db/websrolls`);

var db = new sqlite3.Database(__dirname+`/../db/webscrolls`);


exports.doService = async jsonReq => {
    LOG.info(jsonReq);
    
    db.run(`INSERT INTO LeaveStatus(LeaveID, Response, LeadID, EmpID, Comment, year, IsArchive, createdBy) VALUES( "${jsonReq.LeaveID}","${jsonReq.response}","${jsonReq.leadID}","${jsonReq.EmpID}","${jsonReq.comment}","${jsonReq.year}","0","${jsonReq.createdBy}")`);
    let changeResponse = await updateLeavesAppliedTable(jsonReq);

    let sendEmail = await sendEmailTOSender(jsonReq);

    if(changeResponse) return false;

    return true;
};

let updateLeavesAppliedTable = async function(jsonReq){
    db.get(`SELECT LeadID from LeavesApplied where form_ID=?`,jsonReq.LeaveID, (err, row) => {
        if (err) return console.error(err);

        let storedID = row.LeadID;

        for(var i=0;i<storedID.length;i++)
        {
            if(storedID[i] == jsonReq.leadID)   
                {                    
                    if(i == 0)
                        storedID = storedID.slice(i+1);
                    else if(i == (storedID.length - 1))
                        storedID = storedID.slice(0,i);
                    else    
                        storedID = storedID.slice(0,i)+storedID.slice(i+1);
                }
        }
        LOG.info(storedID);
    db.run(`UPDATE LeavesApplied 
        SET LeadID = "${storedID}"
        WHERE form_ID = "${jsonReq.LeaveID}"`);
    });
    
}

const sendEmailTOSender = async function(jsonReq){
    try {
            var leadName = await getLeadName(jsonReq);

            var LeaveInfo = await getLeaveInfo(jsonReq);

            
    } catch (error) {
        console.error(error);
    }
    
}

const getLeadName = function(jsonReq){
    return new Promise((resolve,reject)=>{
        db.get(`SELECT * FROM userdata WHERE id=?`,jsonReq.leadID, (err, row)=>{
            if(err) return reject(err);

            return resolve(row.emp_name);
        })
    });
}

const getLeaveInfo = function(jsonReq){
    return new Promise((resolve,reject)=>{
        db.get(`SELECT * FROM LeavesApplied WHERE form_ID=?`,jsonReq.LeaveID, (err, row)=>{
            if(err) return reject(err);

            return resolve(row);
        })
    });
}
