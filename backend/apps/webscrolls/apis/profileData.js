const sqlite3 = require('sqlite3').verbose();
const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const crypto = require("crypto");

// const db = require(`${API_CONSTANTS.APP_PATH}/db/websrolls`);
const db = new sqlite3.Database(`${__dirname}/../db/webscrolls`);


exports.doService = async jsonReq=> {
	
	try {

        var DesignationData = await getDesigData();
        var DepartmentData  = await getDeptData();
		LOG.info(DesignationData);
		return { result: true, DesigData: DesignationData, DeptData: DepartmentData };
	} catch (error) {
		return false;
	}	
};

const getDesigData = function(){
    return new Promise((resolve,reject) =>{
        try {
            db.all(`SELECT * FROM Designation`, (err,row) =>{
                if(typeof(row[0]) == "undefined" || err)	return resolve(false);
                return resolve(row);
            })
        } catch (error) 
        {
            return false;
        }
    })
}

const getDeptData = function(){
    return new Promise((resolve,reject) =>{
        try {
            db.all(`SELECT * FROM Department`, (err,row) =>{
                if(typeof(row[0]) == "undefined" || err)	return resolve(false);
                LOG.info(row);
                return resolve(row);
            })
        } catch (error) 
        {
            return false;
        }
    })
}
