const sqlite3 = require('sqlite3').verbose();
const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const crypto = require("crypto");

const db = new sqlite3.Database(`${__dirname}/../db/webscrolls`);


exports.doService = async jsonReq => {
    try 
    {
        if(!validateRequest(jsonReq))   return false;
        var hoildayData = await deleteHolidayData(jsonReq);
        if(!hoildayData)  return {result: false , message: "Unable to delete Holiday Data"};

        return {result: true, message:"Holiday Data removed"};
    } 
    catch (error) {
        return false;
    }
};	

const deleteHolidayData = function(jsonReq){
    try {
        db.run(`DELETE FROM holiday WHERE sno="${jsonReq.sno}"`);

        return true;
    } catch (error) {
        return false;
    }
}

let validateRequest = jsonReq => (jsonReq && jsonReq.sno);
