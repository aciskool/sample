const sqlite3 = require('sqlite3').verbose();
const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const crypto = require("crypto");
const utils = require(`${__dirname}/lib/utils.js`);
// const db = require(`${API_CONSTANTS.APP_PATH}/db/websrolls`);
var fs = require("fs");
var http = require('http');

const path = '../apps/webscrolls/media/media';

exports.doService = async jsonReq => {

    try {
        LOG.info(jsonReq);

        var response = await updateUserdata(jsonReq);

        if(!response)   return{results:false , message:"User not added"} ;

        var imageUpload = await uploadImage(jsonReq);

        if(!imageUpload)    return {results:true , message:"Image not Added"};

        return {results:true, message:"User Data with Image is added"};

        
    } catch (error) {
            return false;
    }
};

let updateUserdata = async function(jsonReq){
    var db = new sqlite3.Database(__dirname+`/../db/webscrolls`);

	try {
          var LeadIds = '';
          for(var i=0;i<jsonReq.LeadInfo.length;i++){
                LeadIds+= jsonReq.LeadInfo[i];
                if(i!=(jsonReq.LeadInfo.length - 1))
                    LeadIds+=",";
            }
          var numberOfLeads = jsonReq.LeadInfo.length;
          var timestamp = utils.getTodayDate();

          db.run(`INSERT INTO userdata(emp_name, DOB, Dept, Designation, DOJ, email, isActive, LeadID, NumberLead,u_username, CreatedAt , createdBy) VALUES( "${jsonReq.emp_name}","${jsonReq.dob}","${jsonReq.dept}","${jsonReq.desig}","${jsonReq.doj}","${jsonReq.email}","1","${LeadIds}","${numberOfLeads}","${jsonReq.username}","${timestamp}","${jsonReq.createdBy}")`);
        
          await updateLoginInfo(jsonReq);
       
          db.run(`INSERT INTO LeaveProvided(CL,PL,AvailableCL,AvailablePL,Complementary,UsedComp) VALUES("3","12","3","12","0","0")`);

          return true;

		  }
	catch (error) {
		console.error(error);
	}
}
      
const updateLoginInfo = async function(jsonReq){

    var db = new sqlite3.Database(__dirname+`/../db/webscrolls`);

    try {
        const timestamp = utils.getTodayDate();
        const u_passwordSecret = utils.uniqid();
        const u_password = sha512(jsonReq.password, u_passwordSecret);
        
        db.run(`INSERT INTO UserLoginInfo(u_username, u_password, u_passwordSecret, CreatedAt, createdBy) VALUES("${jsonReq.username}","${u_password}","${u_passwordSecret}","${timestamp}","${jsonReq.createdBy}")`);


    } catch (error) {
        console.error(error);
    }
}

const uploadImage = function(jsonReq){

    var db = new sqlite3.Database(__dirname+`/../db/webscrolls`);

    db.get(`SELECT * FROM userdata WHERE u_username=?`,jsonReq.username,(err,row) => {
        if(typeof(row) == "undefined" || err)	return false;

        var id = row.id;

        fs.writeFile(path+id+'.txt', jsonReq.image, function(err) {
            if (err) {
               return console.error(err);
            }
    
            var dbData = path+id+'.txt';
    
            db.run(`UPDATE userdata 
            SET Image = "${dbData}"
            WHERE id = "${id}"`);
        
         });

    })

     return true;

}

const sha512 = (password, salt) => crypto.createHmac("sha512", salt).update(password).digest("hex");