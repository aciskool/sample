const sqlite3 = require('sqlite3').verbose();
const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const crypto = require("crypto");

// const db = require(`${API_CONSTANTS.APP_PATH}/db/websrolls`);
const db = new sqlite3.Database(`${__dirname}/../db/webscrolls`);


exports.doService = async jsonReq => {
	
    if (!validateRequest(jsonReq)) return API_CONSTANTS.API_INSUFFICIENT_PARAMS;
	try {
		// LOG.info(jsonReq.u_username.trim());
		var returnData = await getAcceptedLeaves(jsonReq);
        var originalData = await getSavedData(jsonReq);
        while(1){
            for(var i=0;i<returnData.length;i++){
                if(returnData[i].dropdown == "Planned Leave" )
                {
                    let value = originalData.AvailablePL;
                    value = value - returnData[i].no_of_days;
                    db.run(`UPDATE LeaveProvided 
                        SET AvailablePL = "${value}"
                        WHERE id = "${jsonReq.id}"`);
                    db.run(`UPDATE LeavesApplied 
                    SET Updated = "1"
                    WHERE form_id = "${returnData[i].form_ID}"`);
                }
                else if(returnData[i].dropdown == "Casual Leave" )
                {
                    let value = originalData.AvailableCL;
                    value = value - returnData[i].no_of_days;
                    db.run(`UPDATE LeaveProvided 
                        SET AvailableCL = "${value}"
                        WHERE id = "${jsonReq.id}"`);
                    db.run(`UPDATE LeavesApplied 
                    SET Updated = "1"
                    WHERE form_id = "${returnData[i].form_ID}"`);
                }
                else if(returnData[i].dropdown == "Complementary Leave" )
                {
                    let value = (parseFloat(originalData.Complementary) - parseFloat(originalData.UsedComp));
                    value = (parseFloat(originalData.UsedComp) - parseFloat(returnData[i].no_of_days));
                    db.run(`UPDATE LeaveProvided 
                        SET UsedComp = "${value}"
                        WHERE id = "${jsonReq.id}"`);
                    db.run(`UPDATE LeavesApplied 
                    SET Updated = "1"
                    WHERE form_id = "${returnData[i].form_ID}"`);
                }
            }
            break;
    }
		return true;
	} catch (error) {
		return false;
	}	
};

const getAcceptedLeaves = async jsonReq =>{
    return new Promise((resolve, reject) => {
        try {
            db.all(`SELECT * FROM LeavesApplied where id=? AND Status="Accepted" AND Updated="0"`,jsonReq.id, (err, row) => {
                if(typeof(row[0]) == "undefined" || err)	return resolve(false);
                // LOG.info(row);
                return resolve(row);
              });
        } catch (error) {
            return false;
        }
    });
}

const getSavedData = jsonReq =>{
    return new Promise((resolve, reject) => {
        try {
            db.get(`SELECT * FROM LeaveProvided where id=?`,jsonReq.id, (err, row) => {
                if(typeof(row) == "undefined" || err)	return resolve(false);
                // LOG.info(row);
                return resolve(row);
              });
        } catch (error) {
            return false;
        }
    });
}

const validateRequest = jsonReq => (jsonReq && jsonReq.id);