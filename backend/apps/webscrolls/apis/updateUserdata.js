const sqlite3 = require('sqlite3').verbose();
const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const crypto = require("crypto");
const utils = require(`${__dirname}/lib/utils.js`);
// const db = require(`${API_CONSTANTS.APP_PATH}/db/websrolls`);


exports.doService = async jsonReq => {

    try {

        var updateStatus = await updateUserData(jsonReq);
        if(!updateStatus)   return false;

        LOG.info(jsonReq);

        if(jsonReq.newPass) await updatePassword(jsonReq);

        return {results:true , message:"Userdata Updated"};

    } 
    catch (error) 
    {
        return false;
    }


}

const updateUserData = async function(jsonReq){
    var db = new sqlite3.Database(__dirname+`/../db/webscrolls`);

    try 
    {
        var LeadID = "";
        for(var i=0;i<jsonReq.leadInfo.length;i++){
            LeadID+=jsonReq.leadInfo[i];

            if(i != (jsonReq.leadInfo.length - 1) )
                LeadID+= ",";
        }
        db.run(`UPDATE userdata 
        SET emp_name = "${jsonReq.name}",
             DOB = "${jsonReq.dob}",
             DOJ = "${jsonReq.doj}",
             Dept = "${jsonReq.dept}",
             Designation = "${jsonReq.desig}",
             LeadID = "${LeadID}",
             isActive = "${jsonReq.isactive}",
             NumberLead = "${jsonReq.leadInfo.length}"

        WHERE id = "${jsonReq.id}"`);

        return true;
    } 
    catch (error) {
        return false;
    }
    
}

const updatePassword = function(jsonReq){
    try {
        var db = new sqlite3.Database(__dirname+`/../db/webscrolls`);

        var passwordSecret = utils.uniqid();
        var password = sha512(jsonReq.newPass, passwordSecret);
        const timestamp = utils.getTodayDate();
        db.run(`UPDATE UserLoginInfo 
                SET u_password = "${password}",
                    CreatedAt ="${timestamp}",
                    u_passwordSecret ="${passwordSecret}"

                WHERE id = "${jsonReq.id}"`);
    } 
    catch (error) 
    {
        console.error(error);
    }
}

const sha512 = (password, salt) => crypto.createHmac("sha512", salt).update(password).digest("hex");
