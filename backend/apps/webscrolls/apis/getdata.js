const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const sqlite3 = require("sqlite3").verbose();
const db = new sqlite3.Database(`${__dirname}/../db/webscrolls`);

exports.doService = async jsonReq => {
  if (!validateRequest(jsonReq)) return API_CONSTANTS.API_INSUFFICIENT_PARAMS;
  try {
    const login = await getLogin(jsonReq);
    if (!login) return { result: false, message: "Not logged in" };

    const userData = await getUser(login.id);
    LOG.info(userData);
    if (!userData) return { result: false, message: "User data not found" };

    return userData;
  } catch (err) {
    console.error(err);
    return CONSTANTS.FALSE_RESULT;
  }
};

const getLogin = jsonReq => {
  return new Promise((resolve, reject) => {
    LOG.info(jsonReq.u_username);
    db.get(`SELECT * FROM UserLoginInfo where u_username=?`,jsonReq.u_username.trim(), (err, row) => {
      LOG.info(resolve(row));
      if (err) return reject(err);
      return resolve(row);
    });
  });
};

const getUser = userId => {
  return new Promise((resolve, reject) => {
    db.get(`SELECT * FROM userdata where id=?`,userId, (err, row) => {
      if (err) return reject(err);
      return resolve(row);
    });
  });
};

const validateRequest = jsonReq =>
  jsonReq && jsonReq.u_username && jsonReq.u_password;
