const sqlite3 = require('sqlite3').verbose();
const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const crypto = require("crypto");

const db = new sqlite3.Database(`${__dirname}/../db/webscrolls`);


exports.doService = async jsonReq => {
    try 
    {
        var hoildayData = await getHolidayData();
        if(!hoildayData)  return {result: false , message: "Unable to get Holiday Data"};

        return {result: true, message:hoildayData};
    } 
    catch (error) {
        return false;
    }
};	

const getHolidayData = function(){
    return new Promise((resolve,reject)=>{
        db.all(`SELECT * FROM holiday`, (err,row) =>{
            if(typeof(row[0]) == "undefined" || err)	return resolve(false);

            return resolve(row);
        });
    });
};

