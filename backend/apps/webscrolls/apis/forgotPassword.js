const sqlite3 = require('sqlite3').verbose();
const API_CONSTANTS = require(`${__dirname}/lib/constants.js`);
const crypto = require("crypto");

// const db = require(`${API_CONSTANTS.APP_PATH}/db/websrolls`);
const db = new sqlite3.Database(`${__dirname}/../db/webscrolls`);


exports.doService = async jsonReq => {
  
  if (!validateRequest(jsonReq)) return API_CONSTANTS.API_INSUFFICIENT_PARAMS;

	try {
		const response = jsonReq.u_username;
    // LOG.info("Here");
    const email = await getUserEmail(jsonReq);
    LOG.info(email);
		if(!email) return API_CONSTANTS.API_RESPONSE_LOGIN_FAIL;
		const requestChange = await check_email(email, jsonReq);
		
		if (!requestChange) return API_CONSTANTS.API_RESPONSE_CHANGE_PASS_FAIL;
		
		return { result: true, results: response, message: "Send the Request for password change" };
	} catch (error) {
		return false;
	}	
};

const getUserEmail = jsonReq =>{
    return new Promise((resolve, reject) => {
			try {
				db.get(`SELECT * FROM userdata where u_username=? `,jsonReq.name.trim(), (err, row) => {
					if(typeof(row) == "undefined" || err)	return resolve(false);
          LOG.info(row);
					return resolve(row.email);
				  });
			} catch (error) {
				return false;
			}
		});
};

const check_email = function(email, jsonReq){
	try {
		if(email === jsonReq.email.trim())
			return true;
		return false;
	} catch (error) {
		console.error(error);
	}
}

const validateRequest = jsonReq => (jsonReq && jsonReq.name && jsonReq.email);