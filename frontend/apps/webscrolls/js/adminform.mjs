import { loginform } from "../js/loginform.mjs";
import {session} from "/framework/js/session.mjs";
import { router } from "/framework/js/router.mjs";
import { APP_CONSTANTS } from "./constants.mjs";
import { encryptSession } from "../js/lib/encryptSession.mjs";
import { apimanager as apiman } from "/framework/js/apimanager.mjs";

let change = function(){
    try {
        setTimeout(async function(){
            // Function to load the header Data
            let Data = encryptSession.get(APP_CONSTANTS.USER_DATA);
            let LeaveData =  session.get("LeavesAvailable");
    
            let RemainLeave = (LeaveData[0].AvailableCL + LeaveData[0].AvailablePL + (LeaveData[0].Complementary - LeaveData[0].UsedComp)); 
            let TakenLeave = (LeaveData[0].CL - LeaveData[0].AvailableCL) + (LeaveData[0].PL - LeaveData[0].AvailablePL) + LeaveData[0].UsedComp;
            let remain_days = (RemainLeave>1)?"Days":"Day";
            let taken_days = (TakenLeave>1)?"Days":"Day";
            
            let rem_leave = document.querySelector("body > page-generator").shadowRoot.querySelector("#block2").shadowRoot.querySelector("body > div > div.item5.grid-item-extension.class-tout > div");
            rem_leave.innerHTML = (RemainLeave + " " + remain_days);
            let takenLeave = document.querySelector("body > page-generator").shadowRoot.querySelector("#block2").shadowRoot.querySelector("body > div > div.item4.grid-item-extension.class-tin > div");
            takenLeave.innerHTML = (TakenLeave + " " + taken_days);
                
            let todayDate = document.querySelector("body > page-generator").shadowRoot.querySelector("#headerimg").shadowRoot.querySelector("#container > article > table:nth-child(2) > tbody > tr > td:nth-child(2) > p");
            var date_today = loginform.getTodayDate();
            todayDate.innerHTML = date_today;
            
            let applyLeaveButton = document.querySelector("body > page-generator").shadowRoot.querySelector("#headerimg").shadowRoot.querySelector("#container > article > table:nth-child(2) > tbody > tr > td:nth-child(3) > img");
            applyLeaveButton.onclick = function(){
                loginform.showHolidayData();
            }
            setTimeout(function() {
                loginform.sidebarForUser();
            }, 300);

            callAPI();
            loadTableData();
        },900)
    } catch (error) {
        console.error(error);
    }
    
}

let callAPI = async function(id){
    let requestObject = {id : "1"};
    let responseObject = await apiman.rest(APP_CONSTANTS.ADMIN_LEAVE, "POST", requestObject, true);

    await updateColumn(responseObject);
}

let updateColumn = async function(leaveData){
    let data = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("body > div");
    let body = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("body");

    for(var j=0; j<leaveData.length;j++){
        var checkLead = await checkLeadID(leaveData[j].LeadID);
        if(!checkLead) continue;
        let clone = document.createElement("div");
        clone.value = data.value;
        clone = data.cloneNode(true);
        body.appendChild(clone);
    }
    updateBlocks(leaveData);

}

const callLoginAPI = async function(){
    let requestObject = {id : "1"};
    let responseObject = await apiman.rest(APP_CONSTANTS.ADMIN_LEAVE, "POST", requestObject, true);

    let value = await CheckColumns(responseObject);
    return value;
}// Notification Function

const CheckColumns = async function(leaveData){
    var counter = 0;
    for(var j=0; j<leaveData.length;j++){
        var checkLead = await checkLeadID(leaveData[j].LeadID)
        if(!checkLead) continue;
        counter++;
        break;
    }
    return counter;
}// Notification Function

let updateBlocks = async function(data){
    var count = 0;
    for(var i=1,j=0,k=1;i<=data.length;i++,j++){
        var showLeaveCheck = await checkLeadID(data[j].LeadID);
        if(!showLeaveCheck) continue;
        count++;
        let requestObject = {id : data[j].id};
        let responseObject = await apiman.rest(APP_CONSTANTS.DATA_ID, "POST", requestObject, true);

        let name = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("body > div:nth-child("+k+") > div.item1.grid-item-extension.class-name > div");
        name.innerHTML = responseObject.emp_name;

        let imagebyID = {id: data[j].id};
        let imageData = await apiman.rest(APP_CONSTANTS.IMAGE_DATA, "POST", imagebyID, true);
        if(imageData.result!="None" && imageData.result){
            let image = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("body > div:nth-child("+k+") > div.item0.grid-item-extension.class-image > content-post").shadowRoot.querySelector("#container > article > p > a > img")
            image.src = imageData.result;
        }

        let dateFrom = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("body > div:nth-child("+k+") > div.item4.grid-item-extension.class-datefrom > div");
        dateFrom.innerHTML = data[j].date_from;
        let dateTo = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("body > div:nth-child("+k+") > div.item5.grid-item-extension.class-dateto > div");
        dateTo.innerHTML = data[j].date_to;
        let leaveType = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("body > div:nth-child("+k+") > div.item10.grid-item-extension.class-typeText > div");
        leaveType.innerHTML = data[j].dropdown;
        let Type = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("body > div:nth-child("+k+") > div.item11.grid-item-extension.class-type > div");
        Type.innerHTML = data[j].ifhalf;
        let reason = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("body > div:nth-child("+k+") > div.item7.grid-item-extension.class-reasontext > div");
        reason.innerHTML = data[j].reason;
        let HalfType = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("body > div:nth-child("+k+") > div.item13.grid-item-extension.class-halfInfo > div");
        HalfType.innerHTML = data[j].half;
        if(Type.innerHTML == "Full Day"){
            HalfType.innerHTML = "None";
        }
        let accept = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("body > div:nth-child("+k+") > div.item14.grid-item-extension.class-accept > input");
        accept.id = data[j].form_ID;
        accept.data = responseObject.id;
        accept.onclick = function(){
            acceptButton(accept.id,accept.data);
            //call update API from here
        }
        let reject = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("body > div:nth-child("+k+") > div.item15.grid-item-extension.class-reject > input");
        reject.id = data[j].form_ID;
        reject.data = responseObject.id;
        reject.onclick = function(){
            rejectButton(reject.id,reject.data);
            //call update API from here
        }
            k++;
        }
    let extraBlock = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("body > div:nth-child("+(parseInt(count)+1)+")");// to remove the parent Block
    extraBlock.style.display = 'none';
}

let acceptButton = async function(leaveID, empID){

    var acceptOption = document.getElementById("reject");
    var message_accept = document.getElementById("reject_reason");

    acceptOption.style.display = "block";
    
    var ok = document.getElementById("accept");
    var cancel = document.getElementById("cancel");

    ok.onclick = async function(){
        let Data = encryptSession.get(APP_CONSTANTS.USER_DATA);

        let date = new Date();
        let year = date.getFullYear();
        let comment = (message_accept.value!="") ? message_accept.value : "none";

        let requestObject = {leadID:Data.id, response:1, LeaveID:leaveID, EmpID:empID, year:year, comment:comment, createdBy: Data.id};
        let responseObject = await apiman.rest(APP_CONSTANTS.LEAVE_STATUS, "POST", requestObject, true);

        if(responseObject)
        {
            //update api here
            console.log("id using here " + empID);
            await callAPIupdate(empID);

            let responseObject = await apiman.rest(APP_CONSTANTS.EMAIL_TO_SENDER, "POST", requestObject, true);
            LOG.info(responseObject.message);

            location.reload();
        }
    }
    cancel.onclick = function(){
        acceptOption.style.display = "none";
    }
    
}

let rejectButton = async function(leaveID, empID){

    var rejectOption = document.getElementById("reject");
    var message_reject = document.getElementById("reject_reason");

    rejectOption.style.display = "block";
    
    var ok = document.getElementById("accept");
    var cancel = document.getElementById("cancel");

    ok.onclick = function(){
            //
            if(message_reject.value=="") {
                console.log("Provide Reason");
            }
            
            else{
                rejectOptionAPI(message_reject.value, leaveID, empID);
            }
            //update api here
            
        }
    cancel.onclick = function(){
                rejectOption.style.display = "none";
        }
}

let rejectOptionAPI = async function(message,leaveID, empID){

    let Data = encryptSession.get(APP_CONSTANTS.USER_DATA);

    let date = new Date();
    let year = date.getFullYear();
    let comment = message;

    let requestObject = {leadID:Data.id, response:0, LeaveID:leaveID, EmpID:empID,year:year, comment:comment, createdBy: Data.id};
    let responseObject = await apiman.rest(APP_CONSTANTS.LEAVE_STATUS, "POST", requestObject, true);

    if(responseObject)
    {
        await callAPIupdate(empID);

        let responseObject = await apiman.rest(APP_CONSTANTS.EMAIL_TO_SENDER, "POST", requestObject, true);

        LOG.info(responseObject.message);

        location.reload();
    }
}

let checkLeadID = async function(ID){
    var matches = ID.match(/\d+/g); 
    if(matches== null)  return false;

    let Data = encryptSession.get(APP_CONSTANTS.USER_DATA);
    var flag = 0;
    for(var i=0;i<matches.length;i++)
    {
        if(matches[i]==Data.id)
            {
                flag = 1;
                break;
            }
    }
    return flag;
}

let loadTableData = async function(){
    let Data = encryptSession.get(APP_CONSTANTS.USER_DATA);

    let requestObject = {id: Data.id};
    let responseObject = await apiman.rest(APP_CONSTANTS.SUB_INFO, "POST", requestObject, true);

    var name = document.querySelector("body > page-generator").shadowRoot.querySelector("#empLeave").shadowRoot.querySelector("body > div > div.item7.grid-item-extension.class-0\\.5 > div");
    var leaveType = document.querySelector("body > page-generator").shadowRoot.querySelector("#empLeave").shadowRoot.querySelector("body > div > div.item8.grid-item-extension.class-0\\.8 > div");
    var from = document.querySelector("body > page-generator").shadowRoot.querySelector("#empLeave").shadowRoot.querySelector("body > div > div.item9.grid-item-extension.class-0\\.8 > div");
    var to = document.querySelector("body > page-generator").shadowRoot.querySelector("#empLeave").shadowRoot.querySelector("body > div > div.item10.grid-item-extension.class-0\\.8 > div");
    var numberOfDays = document.querySelector("body > page-generator").shadowRoot.querySelector("#empLeave").shadowRoot.querySelector("body > div > div.item11.grid-item-extension.class-0\\.5 > div");
    var status = document.querySelector("body > page-generator").shadowRoot.querySelector("#empLeave").shadowRoot.querySelector("body > div > div.item12.grid-item-extension.class-0\\.5 > div");
    for(var i=0;i<responseObject.returnLength;i++){
        
        var nameTemp = document.createElement("div");
        nameTemp.style.padding = "5px";
        nameTemp.innerHTML = responseObject.message[i].name;
        var leaveTypeTemp = document.createElement("div");
        leaveTypeTemp.style.padding = "5px";
        leaveTypeTemp.innerHTML = responseObject.message[i].leavetype;
        var fromTemp = document.createElement("div");
        fromTemp.style.padding = "5px";
        fromTemp.innerHTML = responseObject.message[i].from;
        var toTemp = document.createElement("div");
        toTemp.style.padding = "5px";
        toTemp.innerHTML = responseObject.message[i].to;
        var numberOfDaysTemp = document.createElement("div");
        numberOfDaysTemp.style.padding = "5px";
        numberOfDaysTemp.innerHTML = responseObject.message[i].noDays;
        let statusTemp = document.createElement("div");
        statusTemp.style.padding ="5px";
        statusTemp.innerHTML = responseObject.message[i].status;
        //
        statusTemp.id = responseObject.message[i].form_ID;
        statusTemp.data = responseObject.message[i].subID;
        statusTemp.style.cursor = "pointer";
        statusTemp.onclick = function(){
            getStatusInfo(statusTemp.id, statusTemp.data);
        }
        //
        name.append(nameTemp);
        leaveType.append(leaveTypeTemp);
        from.append(fromTemp);
        to.append(toTemp);
        numberOfDays.append(numberOfDaysTemp);
        status.append(statusTemp);
    }

const getStatusInfo = async function(formID, subID){
    let requestObject = {id: subID, formID: formID};
    let responseObject = await apiman.rest(APP_CONSTANTS.SHOW_STATUS, "POST", requestObject, true);

    var box = document.getElementById("status");
    var okButton = document.getElementById("ok");

    var oldBox = box.innerHTML;

    var name = document.getElementById("nameID");
    var status = document.getElementById("statusID");
    var reason = document.getElementById("reasonID");
    
    for(var i=0;i<responseObject.message[0].Leads;i++)
        {   
            let tempName = document.createElement("div");
            tempName.innerHTML = responseObject.message[i].name;
            name.append(tempName);
            let tempStatus = document.createElement("div");
            tempStatus.innerHTML = responseObject.message[i].action;
            status.append(tempStatus);
            let tempReason = document.createElement("div");
            if(responseObject.message[i].reason!="none") tempReason.innerHTML = responseObject.message[i].reason;
            
            else tempReason.innerHTML = "-";
            reason.append(tempReason);
            box.style.display = "block";
        }
        okButton.onclick = function(){

            box.style.display = "none";
            box.innerHTML = oldBox;
        }
    }
}

let callAPIupdate = async function(id){
    try {
        let requestObject = {id : id};
        let returnData = await apiman.rest(APP_CONSTANTS.DATA_ID, "POST", requestObject, true);

        let numberLead = returnData.NumberLead;
        console.log("Look here" + id);
        let requestObject2 = {id:id};
        let responseObject = await apiman.rest(APP_CONSTANTS.DETAIL_PENDING, "POST", requestObject2, true);

        console.log(responseObject);
        for(var i=0;i<responseObject.length;i++){
            let leaveId = responseObject[i].form_ID;
            let requestObject = {id:leaveId, numberLeads:numberLead};
            console.log(requestObject);
            let Object = await apiman.rest(APP_CONSTANTS.UPDATE_STATUS, "POST", requestObject, true);
        }
    } catch (error) {
        console.error(error);
    }
}


export const adminform = { change, callLoginAPI };

