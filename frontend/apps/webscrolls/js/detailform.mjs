import {session} from "/framework/js/session.mjs";
import { loginform } from "../js/loginform.mjs";
import { adminform } from "../js/adminform.mjs";
import { router } from "/framework/js/router.mjs";
import { helpers } from "../js/lib/helpers.mjs";
import { apimanager as apiman } from "/framework/js/apimanager.mjs";
import { encryptSession } from "../js/lib/encryptSession.mjs";

let change = function(){
    setTimeout(async function(){
        let Data = encryptSession.get(APP_CONSTANTS.USER_DATA);
        let LeaveData =  session.get("LeavesAvailable");

        callAPIupdate(Data.id);// to update Leave Information
        
        update_data(Data.id);
     
        updateTable(Data.id);

        callAvailableLeavesAPI(Data.id);

        let NotificationStatus = await adminform.callLoginAPI();
        // Notification Icon
        let NotificationIcon = document.querySelector("body > page-generator").shadowRoot.querySelector("#headerimg").shadowRoot.querySelector("#container > article > table:nth-child(2) > tbody > tr > td:nth-child(4) > img");
        if(NotificationStatus == 1){
            NotificationIcon.src = "img/Notification_active.png";
            NotificationIcon.onclick = function(){
                router.loadPage(APP_CONSTANTS.LANDING_HTML);
            }
        }
        let RemainLeave = (LeaveData[0].AvailableCL + LeaveData[0].AvailablePL + (LeaveData[0].Complementary - LeaveData[0].UsedComp)); 
        let TakenLeave = (LeaveData[0].CL - LeaveData[0].AvailableCL) + (LeaveData[0].PL - LeaveData[0].AvailablePL) + LeaveData[0].UsedComp;

        let remain_days = (RemainLeave>1)?"Days":"Day";
        let taken_days = (TakenLeave>1)?"Days":"Day";

        let rem_leave = document.querySelector("body > page-generator").shadowRoot.querySelector("#block2").shadowRoot.querySelector("body > div > div.item5.grid-item-extension.class-tout > div");
        rem_leave.innerHTML = (RemainLeave + " " + remain_days);
        let takenLeave = document.querySelector("body > page-generator").shadowRoot.querySelector("#block2").shadowRoot.querySelector("body > div > div.item4.grid-item-extension.class-tin > div");
        takenLeave.innerHTML = (TakenLeave + " " + taken_days);
        
        let todayDate = document.querySelector("body > page-generator").shadowRoot.querySelector("#headerimg").shadowRoot.querySelector("#container > article > table:nth-child(2) > tbody > tr > td:nth-child(2) > p");
        var date_today = loginform.getTodayDate();
        todayDate.innerHTML = date_today;

        let applyLeaveButton = document.querySelector("body > page-generator").shadowRoot.querySelector("#headerimg").shadowRoot.querySelector("#container > article > table:nth-child(2) > tbody > tr > td:nth-child(3) > img");
            applyLeaveButton.onclick = function(){
                loginform.showHolidayData();
            }
        
        setTimeout(function() {
            loginform.sidebarForUser();
        }, 300);
    },800)
}

let update_data = async function(id){
    let requestObject = {id : id};
    let data = await apiman.rest(APP_CONSTANTS.TABLE_DATA, "POST",requestObject, true);
    
    let allotedData = [data[0].PL, data[0].CL, data[0].Complementary];
    let takenData = [data[0].PL - data[0].AvailablePL, data[0].CL - data[0].AvailableCL, data[0].UsedComp];
    let remainData = [data[0].AvailablePL, data[0].AvailableCL, data[0].Complementary - data[0].UsedComp]

    let typeLeave = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("body > div > div.item5.grid-item-extension.class-01 > div");
    let alottedLeave = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("body > div > div.item6.grid-item-extension.class-0\\.5 > div");
    let leaveTaken = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("body > div > div.item7.grid-item-extension.class-0\\.5 > div");
    let RemainingLeaves = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("body > div > div.item8.grid-item-extension.class-0\\.5 > div");
    let typeOfLeaves = ["Planned", "Casual","Complementary"];
        
    for(var i=0;i<3;i++){
        let option1 = document.createElement("div");
        let option2 = document.createElement("div");
        let option3 = document.createElement("div");

        let type = document.createElement("div");
        type.style.padding = "5px";
        type.innerHTML = typeOfLeaves[i];
        typeLeave.append(type);

        option1.style.padding = "5px";
        option2.style.padding = "5px";
        option3.style.padding = "5px";

        option1.innerHTML = allotedData[i];
        alottedLeave.append(option1);
        option2.innerHTML = takenData[i];
        leaveTaken.append(option2);
        option3.innerHTML = remainData[i];
        RemainingLeaves.append(option3);
    }

}

let updateTable = async function(id){

    let requestObject = {id : id};
    let data = await apiman.rest(APP_CONSTANTS.DETAIL_SHOW, "POST", requestObject, true);
    let dataLength = data.length;
    
    let typeLeave = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform2").shadowRoot.querySelector("body > div > div.item6.grid-item-extension.class-casual > div");
    let leaveFrom = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform2").shadowRoot.querySelector("body > div > div.item7.grid-item-extension.class-casual");
    let leaveTo = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform2").shadowRoot.querySelector("body > div > div.item8.grid-item-extension.class-casual");
    let leaveDays = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform2").shadowRoot.querySelector("body > div > div.item9.grid-item-extension.class-casual");
    let status = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform2").shadowRoot.querySelector("body > div > div.item10.grid-item-extension.class-casual");
    for(var i=0;i<dataLength;i++){
        let type = document.createElement("div");
        type.style.padding = "5px";
        type.innerHTML = data[i].dropdown;
        let dateFrom =  document.createElement("div");
        dateFrom.style.padding = "5px";
        dateFrom.innerHTML = data[i].date_from;
        let dateTo =  document.createElement("div");
        dateTo.style.padding = "5px";
        dateTo.innerHTML = data[i].date_to;
        let noDays =  document.createElement("div");
        noDays.style.padding = "5px";
        noDays.innerHTML = data[i].no_of_days;
        let status_text = document.createElement("div"); 
        status_text.style.padding = "5px";   
        status_text.innerHTML = data[i].Status;
        status_text.id = data[i].form_ID;
        status_text.style.cursor = "pointer";
        status_text.onclick = function(){
            getStatusInfo(status_text.id);
        }

        typeLeave.append(type);
        leaveFrom.append(dateFrom);
        leaveTo.append(dateTo);
        leaveDays.append(noDays);
        status.append(status_text);
    }
}

let callAPIupdate = async function(id){
    let Data = encryptSession.get(APP_CONSTANTS.USER_DATA);
    let numberLead = Data.NumberLead;

    let requestObject = {id : id};
    let responseObject = await apiman.rest(APP_CONSTANTS.DETAIL_SHOW, "POST", requestObject, true);

    for(var i=0;i<responseObject.length;i++){
        let leaveId = responseObject[i].form_ID;
        let requestObject = {id:leaveId, numberLeads:numberLead};
        let Object = await apiman.rest(APP_CONSTANTS.UPDATE_STATUS, "POST", requestObject, true);
    }
}

let callAvailableLeavesAPI = async function(id){// To update Leave Info 
    let requestObject = {id : id};

    let responseObject = await apiman.rest(APP_CONSTANTS.UPDATE_AVAILABLE_LEAVES, "POST", requestObject, true);

    if(responseObject) return true;
}

let getStatusInfo = async function(formID){
    let Data = encryptSession.get(APP_CONSTANTS.USER_DATA);
    let id = Data.id;
    let requestObject = {id: id, formID: formID};
    let responseObject = await apiman.rest(APP_CONSTANTS.SHOW_STATUS, "POST", requestObject, true);

    var box = document.getElementById("status");
    var okButton = document.getElementById("ok");

    var oldBox = box.innerHTML;

    var name = document.getElementById("nameID");
    var status = document.getElementById("statusID");
    var reason = document.getElementById("reasonID");
    
    for(var i=0;i<Data.NumberLead;i++)
        {
            let tempName = document.createElement("div");
            tempName.innerHTML = responseObject.message[i].name;
            name.append(tempName);
            let tempStatus = document.createElement("div");
            tempStatus.innerHTML = responseObject.message[i].action;
            status.append(tempStatus);
            let tempReason = document.createElement("div");
            if(responseObject.message[i].reason!="none") tempReason.innerHTML = responseObject.message[i].reason;
            
            else tempReason.innerHTML = "-";

            reason.append(tempReason);
            box.style.display = "block";
        }
    okButton.onclick = function(){

        box.style.display = "none";
        box.innerHTML = oldBox;
    }
}

export const detailform = { change, callAPIupdate, callAvailableLeavesAPI};