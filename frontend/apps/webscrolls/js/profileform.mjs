import { helpers } from "../js/lib/helpers.mjs";
import { APP_CONSTANTS } from "./constants.mjs";
import { router } from "/framework/js/router.mjs";
import { encryptSession } from "../js/lib/encryptSession.mjs";
import { apimanager as apiman } from "/framework/js/apimanager.mjs";

async function submit(form) {
  var requestObject = {oldPass: form.oldPass, newPass: form.password, confPass: form.confirmPass};

  let message_box = document.getElementById("refill");
  let message = document.getElementById("message_return");

  let confirmation_box = document.getElementById("logout");
  let confiramtion_message = document.getElementById("info");

  let ok_button = document.getElementById("ok");
  ok_button.onclick = function () {
    message_box.style.display = "none";
  };

  let yes_button = document.getElementById("yes");
  let no_button = document.getElementById("no");
  no_button.onclick = function(){
      confirmation_box.style.display = "none";
  }

  if(form.oldPass == "" && form.password=="" && form.confirmPass=="" && form.image == ""){
      message.innerHTML = "Fields are empty!!";
      message_box.style.display = "block";
  }
  else if(form.oldPass == "" && form.password=="" && form.confirmPass=="" && form.image != ""){
    confiramtion_message.innerHTML = "Are you sure you want to update your Avatar?";
    confirmation_box.style.display = "block";
    yes_button.onclick = async function(){
      confirmation_box.style.display = "none";
      let img = document.querySelector("body > page-generator").shadowRoot.querySelector("#profileform").shadowRoot.querySelector("#image").files[0];
      if(img){
          let imgdata = await convertToBase64(img);
          let userData = encryptSession.get(APP_CONSTANTS.USER_DATA);
          let requestObject = {id: userData.id, imageData: imgdata};
          imageUpload(requestObject);
      }
    }
  }
  else{
    callAPI(requestObject);
  }
}

const returnBack = function () {
  router.loadPage(APP_CONSTANTS.ARTICLE_HTML);
};

const callAPI = async function (data) {
  let message_box = document.getElementById("refill");
  let message = document.getElementById("message_return");

  let confirmation_box = document.getElementById("logout");
  let confiramtion_message = document.getElementById("info");

  let ok_button = document.getElementById("ok");
  let no_button = document.getElementById("no");
  let yes_button = document.getElementById("yes");
  ok_button.onclick = function () {
    message_box.style.display = "none";
  };

  no_button.onclick = function(){
    confirmation_box.style.display = "none";
  }

  if (data.newPass == "" || data.confPass == "" || data.oldPass == "") {
    message.innerHTML = "Fields are empty!!";
    message_box.style.display = "block";
  } 
  else if (data.newPass != data.confPass) {
    message.innerHTML = "Passwords donot match!";
    message_box.style.display = "block";
  } 
  else {
    let checkPassword = await checkOldPassword(data.oldPass);
    if (!checkPassword) {
      message.innerHTML = "Old Password is wrong!";
      message_box.style.display = "block";
    } 
    else {
      confiramtion_message.innerHTML = "Are you sure?";
      confirmation_box.style.display = "block";
      yes_button.onclick = async function () {
        console.log("OK");
        message_box.style.display = "none";
        let img = document.querySelector("body > page-generator").shadowRoot.querySelector("#profileform").shadowRoot.querySelector("#image").files[0];
        if(img){
            let imgdata = await convertToBase64(img);
            let userData = encryptSession.get(APP_CONSTANTS.USER_DATA);
            let requestObject = {id: userData.id, imageData: imgdata};
            imageUpload(requestObject);
        }
        await updateUserPassword(data);
      };
    }
  }
};

const checkOldPassword = async function (password) {
  let userData = encryptSession.get(APP_CONSTANTS.USER_DATA);
  let requestObject = { id: userData.id, password: password };
  let responseObject = await apiman.rest(APP_CONSTANTS.CHECK_PASSWORD, "POST", requestObject, true);

  if (responseObject.result) return true;

  return false;
};

const updateUserPassword = async function (data) {
  let userData = encryptSession.get(APP_CONSTANTS.USER_DATA);
  let requestObject = { id: userData.id, password: data.newPass };
  let responseObject = await apiman.rest(APP_CONSTANTS.UPDATE_PASSWORD, "POST", requestObject, true);

  if (responseObject.result) {
    location.reload();
    return { result: true, message: "Password Updated" };
  }
};

//image functions

const getBase64 = async (file, cb) => {
  try {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = async () => {
      // Return the result in the callback
      cb(reader.result);
    };
    reader.onerror = async (error) => {
      console.error(error);
    };
  } catch (error) {
    console.error(error);
  }
};

const convertToBase64 = async (file) => {
  return new Promise(async (resolve, reject) => {
    try {
      await getBase64(file, (data, err) => {
        //handle error
        if (err) {
          throw err;
        }
        //success
        if (data) {
          return resolve(data);
        }
      });
    } catch (error) {
      reject(error);
    }
  });
};

const imageUpload = async function (requestObject) {
    
    let responseObject = await apiman.rest(APP_CONSTANTS.IMAGE_STORE, "POST", requestObject, true);

    if(responseObject.result) console.log("Image Updated");
    else console.log("Error in updating image");
    
    location.reload();
  };



export const profileform = { submit, returnBack };
