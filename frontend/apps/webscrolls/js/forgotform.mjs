import { router } from "/framework/js/router.mjs";
import { apimanager as apiman } from "/framework/js/apimanager.mjs";
import { helpers } from "../js/lib/helpers.mjs";


let submit = async function() {
    let userName = document.querySelector("body > page-generator").shadowRoot.querySelector("#loginform").shadowRoot.querySelector("#u_username");
    let email = document.querySelector("body > page-generator").shadowRoot.querySelector("#loginform").shadowRoot.querySelector("#u_password");

    let requestObject = { name: userName.value, email: email.value};

    let modal_box = document.getElementById("logout");
    let message_modal = document.getElementById("info");
    let yes_button = document.getElementById("yes");
    let no_button = document.getElementById("no");

    no_button.onclick = function(){
        modal_box.style.display = "none";
    }

    let returndata = await apiman.rest(APP_CONSTANTS.FORGOT_PASSWORD, "POST", requestObject, true);
    if(!returndata.result) 
    {
        let message = document.querySelector("body > page-generator").shadowRoot.querySelector("#loginform").shadowRoot.querySelector("body > div > div.item6.grid-item-extension.class-message");
        message.innerHTML = "Provided Credentials do not match*";
        message.style.display = "block";
    }

    else {
        message_modal.innerHTML = "Are you sure you want to get a new password?";
        modal_box.style.display = "block";

        yes_button.onclick = function(){
            callAPItoSendMail(requestObject);
            modal_box.style.display = "none";
            location.reload();
        }
    }        
}

const backToLogin = ()=>{
    router.loadPage(APP_CONSTANTS.MAIN_HTML);
}

const callAPItoSendMail = async function(requestObject){
    let returndata = await apiman.rest(APP_CONSTANTS.SENT_EMAIL, "POST", requestObject, true);
}

export const forgotform = {submit, backToLogin};