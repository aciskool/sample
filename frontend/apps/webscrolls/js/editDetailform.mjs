import { router } from "/framework/js/router.mjs";
import { APP_CONSTANTS } from "./constants.mjs";
import {session} from "/framework/js/session.mjs";
import { apimanager as apiman } from "/framework/js/apimanager.mjs";

const submit = function(form){
    var UserID = session.get("UpdateUserID");

    let modal_box = document.getElementById("refill");
    let message_return = document.getElementById("message_return");
    let ok_button = document.getElementById("ok");

    let modal_box2 = document.getElementById("check");
    let message_return2 = document.getElementById("info");
    let yes_button = document.getElementById("yes");
    let no_button = document.getElementById("no");

    var LeadInfo = getLead();
    console.log(LeadInfo);
    ok_button.onclick = function(){
        modal_box.style.display = "none";
    }

    no_button.onclick = function(){
        modal_box2.style.display = "none";
    }

    if(form.dob == "" || form.emp_name == "" || form.doj == "" || form.dept == "" || form.desig == "" || LeadInfo.length == 0)
    {
        message_return.innerHTML = "Form not completely filled!";
        modal_box.style.display = "block";
    }
    else
    {
    if((form.password == "") || form.password == form.confirmPass) 
        {
            if(form.password == "")    var requestObject = { id: UserID,username: form.username, name: form.emp_name, dob: form.dob, doj: form.doj, dept: form.dept, desig: form.desig,leadInfo: LeadInfo,LeadInfo:LeadInfo, isactive: form.isactive};

            else    var requestObject = { id: UserID ,username:form.username , name: form.emp_name, dob: form.dob, doj: form.doj, dept: form.dept, desig: form.desig,leadInfo: LeadInfo,LeadInfo:LeadInfo, newPass: form.password, confPass: form.confirmPass, isactive: form.isactive};

            message_return2.innerHTML = "Are you sure you want to update User Details?"
            modal_box2.style.display = "block";
            yes_button.onclick = function(){
                updateUserDatainDB(requestObject);
            }

        }     
    else {
            message_return.innerHTML = "Password donot Match"
            modal_box.style.display = "block";
        }
    }

}

const editDetail = function(){
    setTimeout(function(){
        addDeptDropdown();
        setDataForUser();
    },600)
    
}

const addDeptDropdown = async function(){
    let requestObject = {id: "Name"};
    let responseObject = await apiman.rest(APP_CONSTANTS.PROFILE_DATA, "POST", requestObject, true);

    var DefaultUserID = session.get("UpdateUserID");
    let requestObject1 = {id : DefaultUserID};
    let tabledata = await apiman.rest(APP_CONSTANTS.DATA_ID, "POST", requestObject1, true);

    let isactive = document.querySelector("body > page-generator").shadowRoot.querySelector("#editDetail").shadowRoot.querySelector("#isactive");
    let isactiveOption1 = document.createElement("option");
    isactiveOption1.innerHTML = "Yes";
    isactiveOption1.value = 1;
    let isactiveOption2 = document.createElement("option");
    isactiveOption2.innerHTML = "No";
    isactiveOption2.value = 0;

    isactive.append(isactiveOption1);
    isactive.append(isactiveOption2);

    let dept = document.querySelector("body > page-generator").shadowRoot.querySelector("#editDetail").shadowRoot.querySelector("#dept");
    let desig = document.querySelector("body > page-generator").shadowRoot.querySelector("#editDetail").shadowRoot.querySelector("#desig");

    var originalDesig = "";
    
    for(var i=0;i<responseObject.DeptData.length;i++){
        if(responseObject.DeptData[i].Department == tabledata.Dept)     continue;
        var option = document.createElement("option");
        option.innerHTML = responseObject.DeptData[i].Department;
        option.id = responseObject.DeptData[i].Department;
        dept.append(option);
    }
    dept.onclick = function(){ 
            updateDesignationDropdown(dept.value, responseObject.DesigData, desig, originalDesig);
    }
}

const updateDesignationDropdown = function(dept, DesigData, desig, originalDesig){
    desig.innerHTML = originalDesig;
    for(var i=0;i<DesigData.length;i++){
        if(DesigData[i].DeptID == dept){
            var option = document.createElement("option");
            option.innerHTML = DesigData[i].Designation;
            desig.append(option);
        }
    }
}

const setDataForUser = async function(){
    var DefaultUserID = session.get("UpdateUserID");
    let requestObject = {id : DefaultUserID};
    let tabledata = await apiman.rest(APP_CONSTANTS.DATA_ID, "POST", requestObject, true);

    let nameValue = document.querySelector("body > page-generator").shadowRoot.querySelector("#editDetail").shadowRoot.querySelector("#emp_name");
    nameValue.value = tabledata.emp_name;
    
    let emailValue = document.querySelector("body > page-generator").shadowRoot.querySelector("#editDetail").shadowRoot.querySelector("body > div > div.item5.grid-item-extension.class-enterEmail > input");
    emailValue.value = tabledata.email;

    let dobValue = document.querySelector("body > page-generator").shadowRoot.querySelector("#editDetail").shadowRoot.querySelector("#dob");
    dobValue.value = tabledata.DOB;

    let dojValue = document.querySelector("body > page-generator").shadowRoot.querySelector("#editDetail").shadowRoot.querySelector("#doj");
    dojValue.value = tabledata.DOJ;

    let deptValue = document.querySelector("body > page-generator").shadowRoot.querySelector("#editDetail").shadowRoot.querySelector("#dept");
    let deptOption = document.createElement("option");
    deptOption.innerHTML = tabledata.Dept;
    deptValue.append(deptOption);
    
    let deisgValue = document.querySelector("body > page-generator").shadowRoot.querySelector("#editDetail").shadowRoot.querySelector("#desig");
    let option = document.createElement("option");
    option.innerHTML = tabledata.Designation;
    deisgValue.append(option);

    let multipleSelect = document.querySelector("body > page-generator").shadowRoot.querySelector("#editDetail").shadowRoot.querySelector("#leads").shadowRoot.querySelector("#LeadOptions");
    var numberID = tabledata.LeadID.toString().match(/\d+/g).map(Number);

    for(var i=0;i<multipleSelect.length;i++)
    {
        for(var j=0;j<numberID.length;j++){
            if(numberID[j] == multipleSelect[i].value)  multipleSelect[i].selected = true;
        }
    }

    let usernameSelect = document.querySelector("body > page-generator").shadowRoot.querySelector("#editDetail").shadowRoot.querySelector("body > div > div.item18.grid-item-extension.class-enterusername > input");
    usernameSelect.value = tabledata.u_username;

    let isactive = document.querySelector("body > page-generator").shadowRoot.querySelector("#editDetail").shadowRoot.querySelector("#isactive");
    isactive.value = tabledata.isActive;
}

const getLead = function(){
            let select = document.querySelector("body > page-generator").shadowRoot.querySelector("#editDetail").shadowRoot.querySelector("#leads").shadowRoot.querySelector("body > select");
     
            var result = [];
            var options = select && select.options;
            var opt;
          
            for (var i=0, iLen=options.length; i<iLen; i++) {
              opt = options[i];
          
              if (opt.selected) {
                result.push(opt.value || opt.text);
              }
            }
            var LeadInfo = result;
        return LeadInfo;
}

const updateUserDatainDB = async function(requestObject){
    try {
        let responseObject = await apiman.rest(APP_CONSTANTS.UPDATE_USERDATA, "POST", requestObject, true);
        if(!responseObject.message)     console.log("Error in Updating Data");
        
        let updateSubData = await apiman.rest(APP_CONSTANTS.UPDATE_SUB_INFO, "POST", requestObject, true);
        if(!updateSubData.message)      console.log("Error in updating Sub Info");
        location.reload();
    } catch (error) {
        console.error(error);
    }
}

const cancel = function(){
    router.loadPage(APP_CONSTANTS.USER_LIST_HTML);
}

export const editDetailform = {submit, editDetail, cancel};
