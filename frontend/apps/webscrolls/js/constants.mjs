/* 
 * (C) 2015 TekMonks. All rights reserved.
 * License: MIT - see enclosed license.txt file.
 */
const FRONTEND = "http://localhost:8080";
const BACKEND = "http://localhost:9090";
const APP_PATH = `${FRONTEND}/apps/webscrolls`;
const API_PATH = `${BACKEND}/apps/webscrolls`;

export const APP_CONSTANTS = {
    FRONTEND, BACKEND, APP_PATH,
    MAIN_HTML: APP_PATH+"/home.html",
    ARTICLE_HTML: APP_PATH+"/article.html",
    LANDING_HTML: APP_PATH+"/landing.html",
    DETAILS_HTML: APP_PATH+"/detail.html",
    PROFILE_HTML: APP_PATH+"/profile.html",
    EDIT_HTML: APP_PATH+"/editDetail.html",
    FORGOT_HTML:  APP_PATH+"/forgot.html",
    RESET_HTML:  APP_PATH+"/reset.html",
    INDEX_HTML: APP_PATH+"/index.html",
    USER_LIST_HTML: APP_PATH+"/userList.html",
    REGISTER_HTML: APP_PATH+"/register.html",
    ERROR_HTML: FRONTEND+"/framework/error.html",
    CMS_ROOT_URL: `${APP_PATH}/articles`,

    // CMS APIs
    API_CMS_DIR_CONTENTS: API_PATH+"/cmsdirlisting",
    API_NAV_MENU_LISTING: API_PATH+"/navmenulisting",

    SESSION_NOTE_ID: "com_monkshu_app_mnkp",
    USER_DATA: "user_data",
    // Login constants
    MIN_PASS_LENGTH: 8,
    API_LOGIN: `${API_PATH}/login`,
    DATA_STORE: `${API_PATH}/data`,
    ADMIN_LEAVE: `${API_PATH}/adminshow`,
    DETAIL_SHOW: `${API_PATH}/detail`,
    DATA_ID:`${API_PATH}/getdatabyID`,
    LEAVE_STATUS:`${API_PATH}/leavestatus`,
    EMAIL_TO_SENDER:`${API_PATH}/emailToSender`,
    DELETE_HOLIDAY:`${API_PATH}/deleteHoliday`,
    GET_LEAD_DATA:`${API_PATH}/getLeadData`,
    ARCHIVE_DATA:`${API_PATH}/archiveData`,
    SHOW_ARCHIVE_DATA:`${API_PATH}/showArchiveData`,
    UPDATE_COMPLEMENTARY:`${API_PATH}/updateComplementary`,
    SHOW_HOLIDAY:`${API_PATH}/showHoliday`,
    UPDATE_HOLIDAY:`${API_PATH}/updateHoliday`,
    ARCHIVE_YEAR:`${API_PATH}/archiveYear`,
    UPDATE_STATUS:`${API_PATH}/updateStatus`,
    UPDATE_USERDATA:`${API_PATH}/updateUserdata`,
    SENT_EMAIL:`${API_PATH}/sendemail`,
    DETAIL_PENDING: `${API_PATH}/detailshowforpending`,
    UPDATE_AVAILABLE_LEAVES:`${API_PATH}/updateLeaveType`,
    UPDATE_PASSWORD:`${API_PATH}/updatePassword`,
    IMAGE_STORE:`${API_PATH}/imageStore`,
    IMAGE_DATA:`${API_PATH}/imageData`,
    USER_LIST:`${API_PATH}/userlist`,
    PROFILE_DATA:`${API_PATH}/profileData`,
    SUB_INFO:`${API_PATH}/subinfo`,
    CHECK_USERNAME:`${API_PATH}/checkUsername`,
    CHECK_PASSWORD:`${API_PATH}/checkPassword`,
    LEAD_DATA:`${API_PATH}/leadData`,
    ADD_USER_DATA:`${API_PATH}/addUserData`,
    UPDATE_SUB_INFO:`${API_PATH}/updateSubInfo`,
    TABLE_DATA:`${API_PATH}/detail2`,
    GET_DATA: `${API_PATH}/getdata`,
    FORGOT_PASSWORD: `${API_PATH}/forgotpassword`,
    SHOW_STATUS:`${API_PATH}/showstatus`,
    BCRYPT_SALT: "$2a$10$VFyiln/PpFyZc.ABoi4ppf",
    USERID: "id",
    ADMIN_ROLE: "admin",
    HR_ROLE: "hr",
    USER_ROLE: "user",
    GUEST_ROLE: "guest"
}

APP_CONSTANTS.PERMISSIONS_MAP = {
    hr:[APP_CONSTANTS.INDEX_HTML, APP_CONSTANTS.MAIN_HTML, APP_CONSTANTS.FORGOT_HTML, APP_CONSTANTS.ARTICLE_HTML, APP_CONSTANTS.LANDING_HTML, APP_CONSTANTS.DETAILS_HTML, APP_CONSTANTS.ERROR_HTML, APP_CONSTANTS.REGISTER_HTML, APP_CONSTANTS.USER_LIST_HTML, APP_CONSTANTS.EDIT_HTML, APP_CONSTANTS.PROFILE_HTML], 
    admin:[APP_CONSTANTS.INDEX_HTML, APP_CONSTANTS.MAIN_HTML, APP_CONSTANTS.FORGOT_HTML, APP_CONSTANTS.ARTICLE_HTML, APP_CONSTANTS.LANDING_HTML, APP_CONSTANTS.DETAILS_HTML, APP_CONSTANTS.ERROR_HTML, APP_CONSTANTS.REGISTER_HTML, APP_CONSTANTS.USER_LIST_HTML, APP_CONSTANTS.EDIT_HTML,  APP_CONSTANTS.PROFILE_HTML, APP_CONSTANTS.RESET_HTML], 
    user:[APP_CONSTANTS.INDEX_HTML, APP_CONSTANTS.MAIN_HTML, APP_CONSTANTS.FORGOT_HTML, APP_CONSTANTS.ARTICLE_HTML, APP_CONSTANTS.LANDING_HTML, APP_CONSTANTS.DETAILS_HTML, APP_CONSTANTS.ERROR_HTML,  APP_CONSTANTS.PROFILE_HTML], 
    guest:[APP_CONSTANTS.INDEX_HTML, APP_CONSTANTS.MAIN_HTML, APP_CONSTANTS.FORGOT_HTML]
}