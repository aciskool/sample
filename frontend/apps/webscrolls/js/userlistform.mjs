import { router } from "/framework/js/router.mjs";
import { APP_CONSTANTS } from "./constants.mjs";
import { adminform } from "./adminform.mjs";
import { detailform } from "./detailform.mjs";
import { loginform } from "./loginform.mjs";
import {session} from "/framework/js/session.mjs";
import { helpers } from "../js/lib/helpers.mjs";
import { encryptSession } from "../js/lib/encryptSession.mjs";
import { apimanager as apiman } from "/framework/js/apimanager.mjs";


const updateList = async function(){
    var tabledata = await (await fetch(`${APP_CONSTANTS.USER_LIST}`, { method: "GET" })).json();
    
    setTimeout(function(){
        updateTable(tabledata);
        updateHeaderInfo();
    }, 700);
}

const updateTable = function(tabledata){
    let name = document.querySelector("body > page-generator").shadowRoot.querySelector("#userlist").shadowRoot.querySelector("body > div > div.item6.grid-item-extension.class-01");
    let email = document.querySelector("body > page-generator").shadowRoot.querySelector("#userlist").shadowRoot.querySelector("body > div > div.item7.grid-item-extension.class-0\\.5")
    let dept = document.querySelector("body > page-generator").shadowRoot.querySelector("#userlist").shadowRoot.querySelector("body > div > div.item8.grid-item-extension.class-0\\.5");
    let desig = document.querySelector("body > page-generator").shadowRoot.querySelector("#userlist").shadowRoot.querySelector("body > div > div.item9.grid-item-extension.class-0\\.5");
    let editButton = document.querySelector("body > page-generator").shadowRoot.querySelector("#userlist").shadowRoot.querySelector("body > div > div.item10.grid-item-extension.class-edit");
    let addButton = document.querySelector("body > page-generator").shadowRoot.querySelector("#userlist").shadowRoot.querySelector("body > div > div.item11.grid-item-extension.class-add");

    for(var i=0;i<tabledata.UserData.length;i++){
        let nameTemp = document.createElement("div");
        nameTemp.style.padding = "5px";
        let emailTemp = document.createElement("div");
        emailTemp.style.padding = "5px";
        let deptTemp = document.createElement("div");
        deptTemp.style.padding = "5px";
        let desigTemp = document.createElement("div");
        desigTemp.style.padding = "5px";
        let editTemp = document.createElement("div");
        editTemp.style.padding = "5px";
        let addTemp = document.createElement("div");
        addTemp.style.padding = "5px";

        nameTemp.innerHTML = tabledata.UserData[i].emp_name;
        nameTemp.value = tabledata.UserData[i].id;
        emailTemp.innerHTML = tabledata.UserData[i].email;
        deptTemp.innerHTML = tabledata.UserData[i].Dept;
        desigTemp.innerHTML = tabledata.UserData[i].Designation;

        var editimage = new Image();
        editimage.src = "./img/editIcon.png";
        editimage.setAttribute("width", "15");
        editimage.setAttribute("height", "15");

        editTemp.appendChild(editimage);
        editTemp.type = "button";
        // editTemp.setAttribute("src","./img/editIcon.png");
        
        editTemp.value = tabledata.UserData[i].id;

        var img = new Image();
        img.src = "./img/plus.jpg";
        img.setAttribute("width", "15");
        img.setAttribute("height", "15");

        addTemp.appendChild(img);
        addTemp.value = tabledata.UserData[i].id;
        editTemp.onclick = function(){
            var DefaultUserID = editTemp.value;

            session.set("UpdateUserID",DefaultUserID);

            router.loadPage(APP_CONSTANTS.EDIT_HTML);
        }
        
        addTemp.onclick = function(){
            var box = document.getElementById("complementary");
            var info = document.getElementById("emp-info");
            info.innerHTML = "Select the number of Complementary Leaves: ";
            box.style.display = "block";
            var no_of_leaves = document.getElementById("leaves");
            var accept_button = document.getElementById("accept");
            var cancel_button = document.getElementById("cancel_button");
            cancel_button.onclick = function(){
                box.style.display = "none";
            }
            accept_button.onclick = function(){
                var numberLeave = no_of_leaves.value;
                var confirmUpdate = updateComplementaryLeaves(addTemp.value, numberLeave);
                if(confirmUpdate) box.style.display = "none";

                else console.log("error in updating Leaves");
            }
        }

        nameTemp.onclick = function(){
            showLeaveInfo(nameTemp.value);
        }
        var editButtonTemp = document.createElement("div");
        editButtonTemp = addTemp + editTemp;

        name.append(nameTemp);
        email.append(emailTemp);
        dept.append(deptTemp);
        desig.append(desigTemp);
        editButton.append(editTemp);
        addButton.append(addTemp);
    }
}

const updateComplementaryLeaves = async function(id, leave){
    var requestObject = {id: id, leave: leave};
    let responseObject = await apiman.rest(APP_CONSTANTS.UPDATE_COMPLEMENTARY, "POST", requestObject, true);

    if(!responseObject.result)   return false;

    else    return true;   
}

const showLeaveInfo = async function(id)
{
    let requestObject = {id : id};
    let responseObject = await apiman.rest(APP_CONSTANTS.DETAIL_SHOW, "POST", requestObject, true);
    console.log(responseObject);

    var modal_box = document.getElementById("status");
    modal_box.style.display = "block";
    var oldBox = modal_box.innerHTML;

    var ok = document.getElementById("ok_button");
    ok.onclick = function(){
        modal_box.style.display = "none";
        modal_box.innerHTML = oldBox;
    }

    var type = document.getElementById("typeID");
    var from = document.getElementById("fromID");
    var to = document.getElementById("toID");
    var status = document.getElementById("statusID");

    for(var i=0;i<responseObject.length;i++){
        let tempType = document.createElement("div");
        tempType.style.padding = "5px";
        tempType.innerHTML = responseObject[i].dropdown;
        type.append(tempType);

        let tempFrom = document.createElement("div");
        tempFrom.style.padding ="5px";
        tempFrom.innerHTML = responseObject[i].date_from;
        from.append(tempFrom);

        let tempTo = document.createElement("div");
        tempTo.style.padding = "5px";
        tempTo.innerHTML = responseObject[i].date_to;
        to.append(tempTo);

        let tempStatus = document.createElement("div");
        tempStatus.style.padding = "5px";
        tempStatus.innerHTML = responseObject[i].Status;
        status.append(tempStatus);
    }
    
}

const updateHeaderInfo = async function(){

        var Data = encryptSession.get(APP_CONSTANTS.USER_DATA);

        let NotificationStatus = await adminform.callLoginAPI();
        // Notification Icon
        let NotificationIcon = document.querySelector("body > page-generator").shadowRoot.querySelector("#headerimg").shadowRoot.querySelector("#container > article > table:nth-child(2) > tbody > tr > td:nth-child(4) > img");
        if(NotificationStatus == 1){
            NotificationIcon.src = "img/Notification_active.png";
            NotificationIcon.onclick = function(){
                router.loadPage(APP_CONSTANTS.LANDING_HTML);
            }
        }

        let LeaveData =  session.get("LeavesAvailable");

        let RemainLeave = (LeaveData[0].AvailableCL + LeaveData[0].AvailablePL + (LeaveData[0].Complementary - LeaveData[0].UsedComp)); 

            let TakenLeave = (LeaveData[0].CL - LeaveData[0].AvailableCL) + (LeaveData[0].PL - LeaveData[0].AvailablePL) + LeaveData[0].UsedComp;

        let remain_days = (RemainLeave>1)?"Days":"Day";
        let taken_days = (TakenLeave>1)?"Days":"Day";

        let rem_leave = document.querySelector("body > page-generator").shadowRoot.querySelector("#block2").shadowRoot.querySelector("body > div > div.item5.grid-item-extension.class-tout > div");
        rem_leave.innerHTML = (RemainLeave + " " + remain_days);
        
        let takenLeave = document.querySelector("body > page-generator").shadowRoot.querySelector("#block2").shadowRoot.querySelector("body > div > div.item4.grid-item-extension.class-tin > div");
        takenLeave.innerHTML = (TakenLeave + " " + taken_days);
        let todayDate = document.querySelector("body > page-generator").shadowRoot.querySelector("#headerimg").shadowRoot.querySelector("#container > article > table:nth-child(2) > tbody > tr > td:nth-child(2) > p");
        detailform.callAPIupdate(Data.id);
        detailform.callAvailableLeavesAPI(Data.id);
        var date_today = loginform.getTodayDate();
        todayDate.innerHTML = date_today;
        let applyLeaveButton = document.querySelector("body > page-generator").shadowRoot.querySelector("#headerimg").shadowRoot.querySelector("#container > article > table:nth-child(2) > tbody > tr > td:nth-child(3) > img");
            applyLeaveButton.onclick = function(){
                loginform.showHolidayData();
            }
        
        setTimeout(function() {
                loginform.sidebarForUser();
            }, 300);
}


const submit = async function(form){
    console.log(form);
}

export const userlistform = {updateList, submit}