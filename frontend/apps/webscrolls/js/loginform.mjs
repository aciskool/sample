import { securityguard } from "/framework/js/securityguard.mjs";
import {session} from "/framework/js/session.mjs";
import { router } from "/framework/js/router.mjs";
import { APP_CONSTANTS } from "./constants.mjs";
import { contactform } from "./contactform.mjs";
import { detailform } from "./detailform.mjs";
import { adminform } from "./adminform.mjs";
import { encryptSession } from "../js/lib/encryptSession.mjs";
import { apimanager as apiman } from "/framework/js/apimanager.mjs";

var data = {}

function submit(form) {
    data = form;
    handleLogin();
}

let showPassword = async function(){
    let password = document.querySelector("body > page-generator").shadowRoot.querySelector("#loginform").shadowRoot.querySelector("#u_password");
    if(password.type == "password")
        password.type = "text";
    else 
        password.type = "password";
}

let handleLogin = async function () {  
    try {
        const requestObject = { u_username: data.u_username, u_password: data.u_password };
        let responseObject = await apiman.rest(APP_CONSTANTS.API_LOGIN, "POST", requestObject, false, true);
        if (!responseObject) throw new Error("No response from API.");

        if (!responseObject.result) {
            let message = document.querySelector("body > page-generator").shadowRoot.querySelector("#loginform").shadowRoot.querySelector("body > div > div.item8.grid-item-extension.class-message");
            message.innerHTML = "*Wrong Credentials";
            return false;
        }
        
        else{
        let returndata = await apiman.rest(APP_CONSTANTS.GET_DATA, "POST", requestObject, true);

        //update DB data
        await detailform.callAvailableLeavesAPI(returndata.id);
        const tableDataID = {id : returndata.id};
        let responseObject = await apiman.rest(APP_CONSTANTS.TABLE_DATA,"POST", tableDataID, true);

        //update Session Data
        encryptSession.set(APP_CONSTANTS.USER_DATA,returndata);
        
        encryptSession.set("LeavesAvailable",responseObject);
        if(returndata.Dept == "Admin")   securityguard.setCurrentRole(APP_CONSTANTS.ADMIN_ROLE);

        else if(returndata.Dept == "HR")  securityguard.setCurrentRole(APP_CONSTANTS.HR_ROLE);
        
        else securityguard.setCurrentRole(APP_CONSTANTS.USER_ROLE);

        router.loadPage(APP_CONSTANTS.ARTICLE_HTML);
        }
    } catch (error) {
        console.error(error);
        return false;
    }
};

let setdefaultValues = function(){
    let calender = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("#from");
    let date = new Date().getDate();
    let mon = (new Date().getMonth()+1);
    if(mon<10) mon = "0"+mon;
    if(date<10) date = "0"+date;
        let year = new Date().getFullYear();
        calender.defaultValue = year+"-"+mon+"-"+date;
}

let setdefaultValues2 = function(){
    let calender = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("#to");
        let date = new Date().getDate();
        let mon = (new Date().getMonth()+1);
    if(mon<10) mon = "0"+mon;
    if(date<10) date = "0"+date;
        let year = new Date().getFullYear();
        calender.defaultValue = year+"-"+mon+"-"+date;
}

let LoadProfilePage = function(){
    router.loadPage(APP_CONSTANTS.PROFILE_HTML);
}

let setLogoutButton = function(button){
    var confirm_leave = document.getElementById("logout");
    var message_confirm = document.getElementById("info");

    message_confirm.innerHTML = "Are you sure you want to Logout?";
    confirm_leave.style.display = "block";
    
    var yes = document.getElementById("yes");
    var no = document.getElementById("no");

    yes.onclick = function(){
            encryptSession.destroy();
            router.loadPage(APP_CONSTANTS.INDEX_HTML);
        }
    no.onclick = function(){
                confirm_leave.style.display = "none";
        }
}

const getTodayDate = function(){
    let date = new Date();
    let monthText = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec"];
    //
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    
    let monthInText = monthText[date.getMonth()];
    var compDate = monthInText + " " + date.getDate() + ","+" "+date.getFullYear()+" | "+strTime;
    return compDate;
}
const forgot = function(){
    router.loadPage(APP_CONSTANTS.FORGOT_HTML);
}

const sidebarForUser = function(){
    setTimeout(() => {
        try {
            var role = securityguard.getCurrentRole();
            var data = encryptSession.get(APP_CONSTANTS.USER_DATA);
            var desig = data.Designation;
            var flag = 0;
            
            let registerBlock = document.querySelector("body > page-generator").shadowRoot.querySelector("#sidebar").shadowRoot.querySelector("body > div > div.item4.grid-item-extension.class-register");
            let employeeBlock = document.querySelector("body > page-generator").shadowRoot.querySelector("#sidebar").shadowRoot.querySelector("body > div > div.item3.grid-item-extension.class-employee"); 
            let resetBlock = document.querySelector("body > page-generator").shadowRoot.querySelector("#sidebar").shadowRoot.querySelector("body > div > div.item5.grid-item-extension.class-reset");

            var acceptedDesig = ["Team Lead","Project Manager","Senior Project Manager","HR Manager","Manager","Operations","HR Manager","Senior HR Executive"];
            for(var i=0;i<acceptedDesig.length;i++)
            {
                if(desig == acceptedDesig[i])
                    flag = 1;
            }
            if(flag){
                var requestBlock = document.querySelector("body > page-generator").shadowRoot.querySelector("#sidebar").shadowRoot.querySelector("body > div > div.item2.grid-item-extension.class-company");
                requestBlock.style.display = "block";
            }

            if(role== "hr" ){
                employeeBlock.style.display = "block";
                registerBlock.style.display = "block";
            }

            if(role == "admin"){
                employeeBlock.style.display = "block";
                registerBlock.style.display = "block";
                resetBlock.style.display = "block";
            }

        } catch (error) {
            console.error(error);
        }
        
    }, 400);
}

const showHolidayData = async function(){
    let requestObject = {id: "1"};
    let responseObject = await apiman.rest(APP_CONSTANTS.SHOW_HOLIDAY, "POST", requestObject, true);

    let holidayValue = document.getElementById("hoilday_value");
    let dateValue = document.getElementById("dates_value");
    let dayValue = document.getElementById("day_value");

    var holiday_box = document.getElementById("calender");
    holiday_box.style.display = "block";

    var oldBox = holiday_box.innerHTML;

    for(var i=0;i<responseObject.message.length;i++){
        let holidayTemp = document.createElement("div");
        holidayTemp.style.padding = "3px";

        let dateTemp = document.createElement("div");
        dateTemp.style.padding = "3px";

        let dayTemp =document.createElement("div");
        dayTemp.style.padding = "3px";

        let deleteTemp = document.createElement("div");
        deleteTemp.style.padding = "3px";

        holidayTemp.innerHTML = responseObject.message[i].holiday;
        dateTemp.innerHTML = responseObject.message[i].date;
        dayTemp.innerHTML = responseObject.message[i].day;

        holidayValue.append(holidayTemp);
        dateValue.append(dateTemp);
        dayValue.append(dayTemp);
    }

    var submit_button = document.getElementById("submit_button");
    submit_button.onclick = function(){
        holiday_box.style.display = "none";
        holiday_box.innerHTML = oldBox;
        }
}

export const loginform = { submit, showPassword, setLogoutButton, getTodayDate, forgot, sidebarForUser, setdefaultValues, setdefaultValues2, showHolidayData, LoadProfilePage};

