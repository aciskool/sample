import { helpers } from "../js/lib/helpers.mjs";
import { APP_CONSTANTS } from "./constants.mjs";
import { router } from "/framework/js/router.mjs";
import { encryptSession } from "../js/lib/encryptSession.mjs";
import { apimanager as apiman } from "/framework/js/apimanager.mjs";

var LeadInfo = [];

const submit = async function(form){
    let imgdata;

    let img = document.querySelector("body > page-generator").shadowRoot.querySelector("#registerform").shadowRoot.querySelector("#image").files[0];
      if(img){
          imgdata = await convertToBase64(img);
      }
      else{
          imgdata = "None";
      }
    let sessionData = encryptSession.get(APP_CONSTANTS.USER_DATA);
    
    let formData = {emp_name: form.emp_name, email: form.email, dob: form.dob, doj: form.doj, dept: form.dept,LeadInfo: LeadInfo, desig: form.desig, username: form.username,password: form.password, image: imgdata, createdBy: sessionData.id};

    let checkingUsername = await checkUsernameDuringSubmission(form.username);
    let modal_box = document.getElementById("refill");
    let message_return = document.getElementById("message_return");
    let ok_button = document.getElementById("ok");
    let message = document.querySelector("body > page-generator").shadowRoot.querySelector("#registerform").shadowRoot.querySelector("body > div > div.item20.grid-item-extension.class-message");


    ok_button.onclick = function(){
        modal_box.style.display = "none";
    }
    if(form.emp_name.trim() == "" || form.email.trim() == "" || form.dob == "" || form.doj == "" || form.dept == "" || form.desig == "" || form.username.trim() == "" || form.password.trim() == "" || form.confirmPass.trim() == "" || LeadInfo.length == 0)
    {
        console.log("form not filled completely");
        message_return.innerHTML = "Form not completely filled!"
        modal_box.style.display = "block";
    }
    else if(!validateEmail(form.email)){
        console.log("Not an email");
        message_return.innerHTML = "Invalid Email!"
        modal_box.style.display = "block";
    }
    else if(checkingUsername)
    {
        message.innerHTML = "*Not Available";
        message.style.color = "red";
        message.style.display ="block";
    }
    else if(form.password != form.confirmPass){
        console.log("password dont match");
        message_return.innerHTML = "Password doesnot Match!"
        modal_box.style.display = "block";
    }
    else{
        updateDatainDB(formData);
    }
}

const run = async function(){
    setTimeout(async function(){
        
        addDeptDropdown();
        getLead();
        checkUsername();
    },700)
}

const addDeptDropdown = async function(){
        let requestObject = {id:"1"};
        let responseObject = await apiman.rest(APP_CONSTANTS.PROFILE_DATA, "POST", requestObject, true);

        let dept = document.querySelector("body > page-generator").shadowRoot.querySelector("#registerform").shadowRoot.querySelector("#dept");
        let desig = document.querySelector("body > page-generator").shadowRoot.querySelector("#registerform").shadowRoot.querySelector("#desig");
        var originalDesig = desig.innerHTML;
        var DesigDefaultOption = document.createElement("option");
        DesigDefaultOption.innerHTML = "Select Department";
        desig.append(DesigDefaultOption);
        for(var i=0;i<responseObject.DeptData.length;i++){
            var option = document.createElement("option");
            option.innerHTML = responseObject.DeptData[i].Department;
            option.id = responseObject.DeptData[i].Department;
            dept.append(option);
        }
        dept.onclick = function(){ 
                updateDesignationDropdown(dept.value, responseObject.DesigData, desig, originalDesig);
        }
}
const getLead = function(){
    let multiple = document.querySelector("body > page-generator").shadowRoot.querySelector("#registerform").shadowRoot.querySelector("#leads").shadowRoot.querySelector("body > select");
        multiple.onclick = function(){
            getSelectValues(multiple);
        }   
        function getSelectValues(select) {
            var result = [];
            var options = select && select.options;
            var opt;
          
            for (var i=0, iLen=options.length; i<iLen; i++) {
              opt = options[i];
          
              if (opt.selected) {
                result.push(opt.value || opt.text);
              }
            }
            LeadInfo = result;
          }
}
const checkUsername = async function(){
    let message = document.querySelector("body > page-generator").shadowRoot.querySelector("#registerform").shadowRoot.querySelector("body > div > div.item22.grid-item-extension.class-message");
    let checkButton =document.querySelector("body > page-generator").shadowRoot.querySelector("#registerform").shadowRoot.querySelector("body > div > div.item21.grid-item-extension.class-check > input");
        checkButton.onclick = async function(){
            let username = document.querySelector("body > page-generator").shadowRoot.querySelector("#registerform").shadowRoot.querySelector("#username");
            let requestObject = {username: username.value.trim()};
            if(username.value.trim() == ""){
                message.innerHTML = "*Not Available";
                message.style.color = "red";
                message.style.display ="block";
            }
            else{
            let responseObject = await apiman.rest(APP_CONSTANTS.CHECK_USERNAME, "POST", requestObject, true);

            if(responseObject.result){
                message.innerHTML = "*Not Available";
                message.style.color = "red";
                message.style.display ="block";
            }
            else{
                message.innerHTML = "*Available";
                message.style.color = "green";
                message.style.display ="block";
            }
        }
    }
}

const updateDesignationDropdown = function(dept, DesigData, desig, originalDesig){
    desig.innerHTML = originalDesig;
    for(var i=0;i<DesigData.length;i++){
        if(DesigData[i].DeptID == dept){
            var option = document.createElement("option");
            option.innerHTML = DesigData[i].Designation;
            desig.append(option);
        }
    }
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

const checkUsernameDuringSubmission = async function(username){
    let requestObject = {username: username};
    let responseObject = await apiman.rest(APP_CONSTANTS.CHECK_USERNAME, "POST", requestObject, true);

    if(responseObject.result) return true;

    else return false;
}

const updateDatainDB = async function(formData){
    let responseObject = await apiman.rest(APP_CONSTANTS.ADD_USER_DATA, "POST", formData, true);

    if(!responseObject.results)     console.log("Unable to add Data"); 

    else{
      let respondObject = await apiman.rest(APP_CONSTANTS.UPDATE_SUB_INFO, "POST", formData, true);

      if(!respondObject.results) LOG.info("Error in updating sub ID info.");
        console.log("User is successfully added");
        location.reload();
    }
}

const returnBack = function (){
    router.loadPage(APP_CONSTANTS.ARTICLE_HTML);
}

const getBase64 = async (file, cb) => {
    try {
      let reader = new FileReader();
      reader.readAsDataURL(file);
      // reader.onload = function () { //
      reader.onloadend = async () => {
        // Return the result in the callback
        cb(reader.result);
      };
      reader.onerror = async (error) => {
        console.error(error);
      };
    } catch (error) {
      console.error(error);
    }
  };
  
  const convertToBase64 = async (file) => {
    return new Promise(async (resolve, reject) => {
      try {
        await getBase64(file, (data, err) => {
          //handle error
          if (err) {
            throw err;
          }
          //success
          if (data) {
            return resolve(data);
          }
        });
      } catch (error) {
        reject(error);
      }
    });
  };
  

export const registerform = {run, submit, returnBack}