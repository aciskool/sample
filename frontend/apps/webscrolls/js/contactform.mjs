import {session} from "/framework/js/session.mjs";
import { router } from "/framework/js/router.mjs";
import { APP_CONSTANTS } from "./constants.mjs";
import { loginform } from "../js/loginform.mjs";
import { detailform } from "../js/detailform.mjs";
import { adminform } from "../js/adminform.mjs";
import { encryptSession } from "../js/lib/encryptSession.mjs";
import { apimanager as apiman } from "/framework/js/apimanager.mjs";

var data = {}

async function submit(form) {
    data = form;

    var holidays = await getHolidayDates();

    let total_days = calculate_weekend();
    let holiday_count = calculate_holidays(holidays);
    var d1=new Date(data.from);
    var d2=new Date(data.to);
    
    let flag = d1<=d2?1:0;
    let Data = encryptSession.get(APP_CONSTANTS.USER_DATA);
    let leaveData = session.get("LeavesAvailable");
    data.id = Data.id;

    var isAllowed = checkifAllowed(form);
    
    let remain_pl = leaveData[0].AvailablePL;
    let remain_cl = leaveData[0].AvailableCL;
    let remain_comp = (parseFloat(leaveData[0].Complementary) - parseFloat(leaveData[0].UsedComp));
    
    var confirm_leave = document.getElementById("confirmation");
    var message_redo = document.getElementById("refill");

    var accept = document.getElementById("accept");
    var cancel = document.getElementById("cancel");

    var message_confirm = document.getElementById("information");
    var return_redo = document.getElementById("message_return");
    
    var half;
    if(data.ifhalf == "Half Day")    half =(0.5*total_days);
    
    else half = 0;

    var ok = document.getElementById("ok");

    var days_applying = (total_days - holiday_count - half ) ;
    data.no_of_days = days_applying;
    var days = (days_applying>1)?days = "days":days="day";
    try {
        if(data.reason == ""){
            return_redo.innerHTML = "Reason is not provided";
                message_redo.style.display = "block";
                ok.onclick = function(){
                    message_redo.style.display = "none";
                }
        }
        else if(days_applying == 0)
        {
            return_redo.innerHTML = "Days applying for is zero!";
            message_redo.style.display = "block";
            ok.onclick = function(){
                message_redo.style.display = "none";
            }
        }
        else if(((days_applying>remain_pl) &&  data.dropdown== "Planned Leave") || (days_applying>remain_cl && data.dropdown== "Casual Leave") || (days_applying>remain_comp && data.dropdown== "Complementary Leave") )
        {
            return_redo.innerHTML = "Not enough "+ data.dropdown+"'s left";
            message_redo.style.display = "block";
            ok.onclick = function(){
                message_redo.style.display = "none";
            }
        }
        else if(days_applying>1 && data.dropdown == "Casual Leave"){
            return_redo.innerHTML = "Only 1 day Casual Leave allowed";
            message_redo.style.display = "block";
            ok.onclick = function(){
                message_redo.style.display = "none";
            }
        }
        else if(data.ifhalf=="Half Day" && data.halve =="None")
        {
            return_redo.innerHTML = "Select any half";
            message_redo.style.display = "block";
            ok.onclick = function(){
                message_redo.style.display = "none"
            }
        }
        else if(!isAllowed){
            return_redo.innerHTML = "Cannot apply with such short notice.";
            message_redo.style.display = "block";
            ok.onclick = function(){
                message_redo.style.display = "none"
            }
        }
        else if(flag==1)
        {
            message_confirm.innerHTML = "You are applying for "+days_applying+" "+days+" of "+data.dropdown;
            confirm_leave.style.display = "block";
            accept.onclick = function(){
                applyForLeave();
            }
            cancel.onclick = function(){
                confirm_leave.style.display = "none";
            }
            
        }
        else if(flag == 0){
            return_redo.innerHTML = "Invalid Input";
            message_redo.style.display = "block";
            ok.onclick = function(){
                message_redo.style.display = "none";
            }
        }
        else 
        {
            message_confirm.innerHTML = "You are applying for "+days_applying+" "+ days+" of "+data.dropdown;
            confirm_leave.style.display = "block";
            accept.onclick = function(){
                applyForLeave();
            }
            cancel.onclick = function(){
                confirm_leave.style.display = "none";
            }
            
        }
    } catch (error) {
        throw(error);
    }
}

const getHolidayDates = async function(){
    let requestObject = {id: "1"};
    let responseObject = await apiman.rest(APP_CONSTANTS.SHOW_HOLIDAY, "POST", requestObject, true);
    
    var holiday = new Array();

        for(var i=0;i<responseObject.message.length;i++){
            holiday[i] = responseObject.message[i].date;
        }

    return holiday;
}

const checkifAllowed = function(form){
    var leaveType = form.dropdown;
    var fromDate = form.from;

    var date = new Date();
    var date2 = new Date(fromDate);
    const diffTime = Math.abs(date2 - date);
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

    if(leaveType == "Planned Leave") {

        if(diffDays < 10)
            return false;
        else
            return true;
    }
    else if(leaveType == "Casual Leave"){

        if(diffDays < 2)
            return false;
        else
            return true;
    }
    else if(leaveType == "WFH"){
        if(diffDays < 3)    
            return false;
        else
            return true;
    }
    else if(leaveType == "Complementary Leave"){
        if(diffDays < 2)    
            return false;
        else
            return true;
    }
    else
        return true;
}

let calculate_weekend = function(){
    var d1=new Date(data.from);
    var d2=new Date(data.to);
    
    for(var i=0; d1<=d2; ) 
    {
        if( d1.getDay() >0 && d1.getDay() <6) i++;
        d1.setDate(d1.getDate()+1) ;
    }
    
    return i;
}

let calculate_holidays = function(holidays){
    var dd1 = new Date(data.from);
    var dd2 = new Date(data.to);
    var holiday_count = 0;
    for(var i=0; dd1<=dd2;){
        for(var j=0;j<holidays.length;){
            var check_data = new Date(holidays[j]);
            if(check_data.getTime() === dd1.getTime()) holiday_count++;
            j++;
        }
        dd1.setDate(dd1.getDate()+1) ;
    }
    return holiday_count;
}

let loadLeaveDropdown = function(){
    var dropdown = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("#dropdown");
    let typeLeaves = ["Planned Leave","Casual Leave","Unpaid Leave","WFH","Complementary Leave"];
    for(var i=0;i<typeLeaves.length;i++){
        var option = document.createElement("option");
        option.innerHTML = typeLeaves[i];
        dropdown.append(option);
    }
}
let selectHalfData = function(){
    var dropdown = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("#halve");
    let typeHalf = ["None","First Half","Second Half"];
    for(var i=0;i<typeHalf.length;i++){
        var option = document.createElement("option");
        option.innerHTML = typeHalf[i];
        dropdown.append(option);
    }
}

let loadDayDropdown = function(){
    var dropdown = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("#ifhalf");
    let typeDays = ["Full Day","Half Day"];
    for(var i=0;i<typeDays.length;i++){
        var option = document.createElement("option");
        option.innerHTML = typeDays[i];
        dropdown.append(option);
    }    
}

let calculate_days = function(){
    var date_to = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("#to");
    var date_from = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("#from");
    var no_of_days = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("#no_of_days");
    var type_of_leave = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("#ifhalf");

    if((date_to.value)===null || (date_from.value)===null){
        no_of_days.value = "Not found";
    }
    if((date_to.value)!=null || (date_from.value)!=null){
        var date1 = (date_to.value[8]) + date_to.value[9];
        var date2 = (date_from.value[8]) + date_from.value[9];
        var days_applying = date1 - date2;
        console.log(type_of_leave);
        if(days_applying<0){
            no_of_days.value = "Invalid";
        }
        else{
        no_of_days.value = (days_applying+1);
        }
    }

}

let applyForLeave = async function () {

        let sessionData = encryptSession.get(APP_CONSTANTS.USER_DATA);
    
        try {
            let requestObject = {dropdown: data.dropdown,from: data.from,to: data.to, ifhalf:data.ifhalf ,no_of_days: data.no_of_days,reason:data.reason , id: data.id, half:data.halve, leadID:sessionData.LeadID, Status:"Pending", createdBy: sessionData.id};
            let responseObject = await apiman.rest(APP_CONSTANTS.DATA_STORE, "POST", requestObject, true);

            if (!responseObject)    throw new Error("No response from API.");

            else    location.reload();
        } catch (error) {
            console.error(error);
            return false;
        }
};
let reset = function(){
    setTimeout(async function(){
        headerData();
        let LeaveData =  encryptSession.get("LeavesAvailable");

        let RemainLeave = (LeaveData[0].AvailableCL + LeaveData[0].AvailablePL + (LeaveData[0].Complementary - LeaveData[0].UsedComp)); 

        let remain_days = (RemainLeave>1)?"Days":"Day";

        let remLeave = document.querySelector("body > page-generator").shadowRoot.querySelector("#contactform").shadowRoot.querySelector("body > div > div.item4.grid-item-extension.class-rem_leave > input");
        remLeave.value = (RemainLeave + " " + remain_days);

        let NotificationStatus = await adminform.callLoginAPI();
        // Notification Icon
        let NotificationIcon = document.querySelector("body > page-generator").shadowRoot.querySelector("#headerimg").shadowRoot.querySelector("#container > article > table:nth-child(2) > tbody > tr > td:nth-child(4) > img");
        if(NotificationStatus == 1){
            NotificationIcon.src = "img/Notification_active.png";
            NotificationIcon.onclick = function(){
                router.loadPage(APP_CONSTANTS.LANDING_HTML);
            }
        }        
        loadLeaveDropdown();
        loadDayDropdown();
        selectHalfData();

        setTimeout(function() {
            loginform.sidebarForUser();
        }, 400);
        
    },800)
}

const headerData = async function(){
    try {
        let Data = encryptSession.get(APP_CONSTANTS.USER_DATA);

        let LeaveData =  encryptSession.get("LeavesAvailable");
        let RemainLeave = (LeaveData[0].AvailableCL + LeaveData[0].AvailablePL + (LeaveData[0].Complementary - LeaveData[0].UsedComp)); 
        let TakenLeave = (LeaveData[0].CL - LeaveData[0].AvailableCL) + (LeaveData[0].PL - LeaveData[0].AvailablePL) + LeaveData[0].UsedComp;
        let remain_days = (RemainLeave>1)?"Days":"Day";
        let taken_days = (TakenLeave>1)?"Days":"Day";

        let rem_leave = document.querySelector("body > page-generator").shadowRoot.querySelector("#block2").shadowRoot.querySelector("body > div > div.item5.grid-item-extension.class-tout > div");
        rem_leave.innerHTML = (RemainLeave + " " + remain_days);
        let takenLeave = document.querySelector("body > page-generator").shadowRoot.querySelector("#block2").shadowRoot.querySelector("body > div > div.item4.grid-item-extension.class-tin > div");
        takenLeave.innerHTML = (TakenLeave + " " + taken_days);
        let todayDate = document.querySelector("body > page-generator").shadowRoot.querySelector("#headerimg").shadowRoot.querySelector("#container > article > table:nth-child(2) > tbody > tr > td:nth-child(2) > p");
        detailform.callAPIupdate(Data.id);
        detailform.callAvailableLeavesAPI(Data.id);
        todayDate.innerHTML = loginform.getTodayDate();
        
        loginform.setdefaultValues();
        loginform.setdefaultValues2();
        let applyLeaveButton = document.querySelector("body > page-generator").shadowRoot.querySelector("#headerimg").shadowRoot.querySelector("#container > article > table:nth-child(2) > tbody > tr > td:nth-child(3) > img");
            applyLeaveButton.onclick = function(){
                loginform.showHolidayData();
            }
    } catch (error) {
        console.error(error);
    }

}



let refresh = function(){
    location.reload();
}

export const contactform = { submit, reset, loadLeaveDropdown,refresh, loadDayDropdown, selectHalfData, calculate_days };