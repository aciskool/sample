import { router } from "/framework/js/router.mjs";
import { APP_CONSTANTS } from "./constants.mjs";
import { helpers } from "../js/lib/helpers.mjs";
import { encryptSession } from "../js/lib/encryptSession.mjs";
import { apimanager as apiman } from "/framework/js/apimanager.mjs";


const loadData = function(){
    setTimeout(() => {
        var archiveYear = document.querySelector("body > page-generator").shadowRoot.querySelector("#editDetail").shadowRoot.querySelector("#year");
        uploadDatatoYearDropdown(archiveYear);

        uploadArchivedYear();
        showHolidayData();
    }, 500);
    
}

const showHolidayData = async function(){
    let requestObject = {id: "1"};
    let responseObject = await apiman.rest(APP_CONSTANTS.SHOW_HOLIDAY, "POST", requestObject, true);

    // console.log(responseObject.message[0]);

    let sno = document.querySelector("body > page-generator").shadowRoot.querySelector("#holidayform").shadowRoot.querySelector("body > div > div.item6.grid-item-extension.class-01");
    sno.innerHTML = "";
    let holiday = document.querySelector("body > page-generator").shadowRoot.querySelector("#holidayform").shadowRoot.querySelector("body > div > div.item7.grid-item-extension.class-0\\.5");
    holiday.innerHTML = "";
    let date = document.querySelector("body > page-generator").shadowRoot.querySelector("#holidayform").shadowRoot.querySelector("body > div > div.item8.grid-item-extension.class-0\\.5");
    date.innerHTML = "";
    let day = document.querySelector("body > page-generator").shadowRoot.querySelector("#holidayform").shadowRoot.querySelector("body > div > div.item9.grid-item-extension.class-0\\.5");
    day.innerHTML = "";
    let deleteButton = document.querySelector("body > page-generator").shadowRoot.querySelector("#holidayform").shadowRoot.querySelector("body > div > div.item10.grid-item-extension.class-0\\.5");
    deleteButton.innerHTML = "";

    for(var i=0,j=1;i<responseObject.message.length; i++){
        let snoTemp = document.createElement("div");
        snoTemp.style.padding = "5px";

        let holidayTemp = document.createElement("div");
        holidayTemp.style.padding = "5px";

        let dateTemp = document.createElement("div");
        dateTemp.style.padding = "5px";

        let dayTemp = document.createElement("div");
        dayTemp.style.padding = "5px";

        let deleteButtonTemp = document.createElement("div");
        deleteButtonTemp.style.padding = "5px";

        snoTemp.innerHTML = j;
        holidayTemp.innerHTML = responseObject.message[i].holiday;
        dateTemp.innerHTML = responseObject.message[i].date;
        dayTemp.innerHTML = responseObject.message[i].day;

        var editimage = new Image();
        editimage.src = "./img/dustbin.png";
        editimage.setAttribute("width", "15");
        editimage.setAttribute("height", "15");

        deleteButtonTemp.appendChild(editimage);
        deleteButtonTemp.value = responseObject.message[i].sno;

        deleteButtonTemp.onclick = function(){
            deleteHolidayData(deleteButtonTemp.value);
        }

        sno.append(snoTemp);
        holiday.append(holidayTemp);
        date.append(dateTemp);
        day.append(dayTemp);
        deleteButton.append(deleteButtonTemp);
        j++;
    }

    var addimage = new Image();
        addimage.src = "./img/plus.jpg";
        addimage.setAttribute("width", "30");
        addimage.setAttribute("height", "30");
    addimage.onclick = function(){
        addHoliday();
    }

    sno.append(addimage);
}

const addHoliday = function(){
    var modal_box = document.getElementById("calender");
    modal_box.style.display = "block";
    
    var ok_button = document.getElementById("submit_button");
    var cancel_button = document.getElementById("cancel");

    cancel_button.onclick = function(){
        modal_box.style.display = "none";
    }

    ok_button.onclick = function(){
        var name = document.getElementById("name_holiday").value;
        var date = document.getElementById("date_holiday").value;

        var sessionData = encryptSession.get(APP_CONSTANTS.USER_DATA);

        var new_date = new Date(date);
        var day = new_date.getDay();
        var day_value = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
        var message = document.getElementById("message");
        var createdBy = sessionData.id;
        if(name=="" || date=="" || message=="") message.innerHTML="Required Info not provided!";

        else{
            var requestObject = {name: name, date: date, day: day_value[day], createdBy: createdBy};
            uploadHolidayinDB(requestObject);
        }
    }
}

const deleteHolidayData =async function(serialValue){
    var modal_box = document.getElementById("logout");
    var yes_button = document.getElementById("yes");
    var no_button = document.getElementById("no");
    var message = document.getElementById("info");

    message.innerHTML = "Are you sure?";

    modal_box.style.display = "block";

    no_button.onclick = function(){
        modal_box.style.display = "none";
    }

    yes_button.onclick = async function(){
        var requestObject = {sno : serialValue};
        let responseObject = await apiman.rest(APP_CONSTANTS.DELETE_HOLIDAY, "POST", requestObject, true);

        if(responseObject.result)   location.reload();

        else console.log("Error in deleting data");
    }

    
}

const uploadHolidayinDB = async function(requestObject){
    let responseObject = await apiman.rest(APP_CONSTANTS.UPDATE_HOLIDAY, "POST", requestObject, true);

    if(responseObject.result){
        location.reload();
    }
}

const uploadDatatoYearDropdown = async function(archiveYear){
    var requestObject = {id : "1"};
    let archiveYearData = await apiman.rest(APP_CONSTANTS.ARCHIVE_YEAR, "POST", requestObject, true);

    var flag = 0;
    var YearToShow = [];

    var date = new Date(2020,1,1);
    var thisDate = new Date();
    var year = date.getFullYear();
    var thisYear = thisDate.getFullYear();
    for(var i=year,m=0;i<=thisYear;i++){
        flag = 0;
        for(var j=0;j<archiveYearData.message.length;j++){
            if(archiveYearData.message[j].Year == i){
                flag =1;
                break;
            }
        }
        if(flag!=1){
            YearToShow[m] = i;
            m++;
        }
    }

    for(var i=0; i<YearToShow.length ; i++)
    {
        let option = document.createElement("option");
        option.innerHTML = YearToShow[i];
        archiveYear.append(option);
    }
}

const uploadArchivedYear = async function(){
    var requestObject = {id : "1"};
    let archiveYearData = await apiman.rest(APP_CONSTANTS.ARCHIVE_YEAR, "POST", requestObject, true);

    var yearDropdown = document.querySelector("body > page-generator").shadowRoot.querySelector("#editDetail").shadowRoot.querySelector("#year2");

    for(var i=0;i<archiveYearData.message.length;i++){
        let option = document.createElement("option");
        option.innerHTML = archiveYearData.message[i].Year;

        yearDropdown.append(option);
    }

}

const archiveYear = async function(){
    var archiveYear = document.querySelector("body > page-generator").shadowRoot.querySelector("#editDetail").shadowRoot.querySelector("#year");
    var yearToArchive = archiveYear.value;

    if(yearToArchive == ""){

    }
    else{
        var modal_box = document.getElementById("logout");
        var modal_message = document.getElementById("info");

        var yes_button = document.getElementById("yes");
        var no_button = document.getElementById("no");

        modal_message.innerHTML = "Are you sure you want to archive "+yearToArchive +"'s leave information?";
        modal_box.style.display = "block";

        no_button.onclick = function(){
            modal_box.style.display = "none";
        }

        yes_button.onclick = async function(){
            var sendArchiveRequest = await archiveData(yearToArchive);

            if(!sendArchiveRequest)     console.log("Data not archived");
            //false case is not done.
            location.reload();
        }
    }
    
}

const archiveData = async function(year){
    var requestObject = {year: year};
    var responseObject = await apiman.rest(APP_CONSTANTS.ARCHIVE_DATA, "POST", requestObject, true);

    if(responseObject.result)  return true;

    return false;
}

const showdata = async function(){
    let yearDropdown = document.querySelector("body > page-generator").shadowRoot.querySelector("#editDetail").shadowRoot.querySelector("#year2");

    let requestObject = {year: yearDropdown.value};
    var responseObject = await apiman.rest(APP_CONSTANTS.SHOW_ARCHIVE_DATA, "POST", requestObject, true);

    var tabledata = await (await fetch(`${APP_CONSTANTS.USER_LIST}`, { method: "GET" })).json();

    var name = {};

    for(var i=0;i<tabledata.UserData.length;i++){
        name[tabledata.UserData[i].id] = tabledata.UserData[i].emp_name;
    }
    let emp_name = document.querySelector("body > page-generator").shadowRoot.querySelector("#detailform").shadowRoot.querySelector("body > div > div.item6.grid-item-extension.class-01");
    emp_name.innerHTML = "";
    let pl_left = document.querySelector("body > page-generator").shadowRoot.querySelector("#detailform").shadowRoot.querySelector("body > div > div.item7.grid-item-extension.class-0\\.5");
    pl_left.innerHTML = "";
    let cl_left = document.querySelector("body > page-generator").shadowRoot.querySelector("#detailform").shadowRoot.querySelector("body > div > div.item8.grid-item-extension.class-0\\.5");
    cl_left.innerHTML = "";
    let compLeave = document.querySelector("body > page-generator").shadowRoot.querySelector("#detailform").shadowRoot.querySelector("body > div > div.item9.grid-item-extension.class-0\\.5");
    compLeave.innerHTML = "";
    let availed = document.querySelector("body > page-generator").shadowRoot.querySelector("#detailform").shadowRoot.querySelector("body > div > div.item10.grid-item-extension.class-0\\.5");
    availed.innerHTML = "";

    for(var i=0;i<responseObject.message.length;i++){
        let nameTemp = document.createElement("div");
        nameTemp.style.padding = "5px";
        let plTemp = document.createElement("div");
        plTemp.style.padding = "5px";
        let clTemp = document.createElement("div");
        clTemp.style.padding = "5px";
        let compTemp = document.createElement("div");
        compTemp.style.padding = "5px";
        let availTemp = document.createElement("div");
        availTemp.style.padding = "5px";
        
        nameTemp.innerHTML = name[responseObject.message[i].id];
        emp_name.append(nameTemp);

        plTemp.innerHTML = responseObject.message[i].pl_left;
        pl_left.append(plTemp);

        clTemp.innerHTML = responseObject.message[i].cl_left;
        cl_left.append(clTemp);

        compTemp.innerHTML = responseObject.message[i].Complementary ;
        compLeave.append(compTemp);

        availTemp.innerHTML = responseObject.message[i].UsedComp ;
        availed.append(availTemp);
    }
}

const backButton = function(){
    router.loadPage(APP_CONSTANTS.ARTICLE_HTML);
}

export const resetform = {loadData, archiveYear, showdata, backButton};