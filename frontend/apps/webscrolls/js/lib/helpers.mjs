import { common } from "/apps/webscrolls/js/common.mjs";

/**
 * Data encryption/decryption wrapper function for Fetch API calls
 * 
 * @param {Object} requestParams - Request parameters for the API call
 * @param {String} requestParams.apiPath - Path for requested API
 * @param {Object} requestParams.apiPayload - Request payload object
 * @param {String} requestParams.apiKey - Key for the requested API
 * @returns {Promise<object>}
 */
const secureFetch = async (requestParams) => {
    try {
        // Validate parameters
        if (!requestParams.apiPath || !requestParams.apiPayload || !requestParams.apiKey) throw new Error("Insufficient parameters.");
        const requestMethod = (requestParams.requestMethod) ? requestParams.requestMethod : "POST";

        // Set the API key
        requestParams.apiPayload.org_monkshu_apikey = requestParams.apiKey;
        const requestObject = { data: await window.encrypt(JSON.stringify(requestParams.apiPayload)) };
        const responseObject = await (await fetch(requestParams.apiPath, { method: requestMethod, body: JSON.stringify(requestObject) })).json();

        if (!responseObject.data) throw new Error("Unexpected response from API.");
        const decryptedResponseObject = await JSON.parse(await window.decrypt(responseObject.data));

        // Logout if timeout message
        if (!decryptedResponseObject.result && decryptedResponseObject.message == "Timeout.") await common.logout();

        return decryptedResponseObject;
    } catch (error) {
        throw error;
    }
};

export const helpers = { secureFetch };