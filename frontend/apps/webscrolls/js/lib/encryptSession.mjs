import { session as monkshuSession } from "/framework/js/session.mjs";

const secret = "*#l,,Gpq]ZGaO7Y";

const set = (id, data) => {
    if (typeof data === 'object') monkshuSession.set(id, data);
    else {
        const test = document.createElement("div");
        test.setAttribute("id", CryptoJS.AES.encrypt(data, secret));
        monkshuSession.set(id, test.id);
    }
}

const get = (id) => {
    if (typeof monkshuSession.get(id) === 'object') return monkshuSession.get(id);
    const sessionData = monkshuSession.get(id);
    if (sessionData)
        return CryptoJS.AES.decrypt(sessionData, secret).toString(CryptoJS.enc.Utf8);
    else
        return sessionData;
}

const remove = (id) => monkshuSession.remove(id);
const destroy = () => monkshuSession.destroy();
const contains = (id) => monkshuSession.contains(id);
const keys = () => monkshuSession.keys();

export const encryptSession = { set, get, remove, destroy, contains, keys };