import { APP_CONSTANTS } from "./constants.mjs";
import {session} from "/framework/js/session.mjs";
import { application } from "./application.mjs";



/** 
 * Async function to upgrade MDL
 * @returns {boolean}
 */
let upgradeMDL = async () => {
    try {
        await $$.require(`${APP_CONSTANTS.APP_PATH}/3p/material.min.js`);
        componentHandler.upgradeAllRegistered();
        return true;
    } catch (error) {
        console.error(error);
        return false;
    }
};

/**
 * Logout action performed using this function.
 */
let logoutAction = () => {
    session.destroy();
    application.init();
    application.main();
    return true;
};

export const common = { upgradeMDL, logoutAction};