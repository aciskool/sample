/* 
 * (C) 2019 TekMonks. All rights reserved.
 * License: MIT - see enclosed license.txt file.
 */
import {i18n} from "/framework/js/i18n.mjs";
import {util} from "/framework/js/util.mjs";
import {router} from "/framework/js/router.mjs";
import { encryptSession } from "../../js/lib/encryptSession.mjs";
import {monkshu_component} from "/framework/js/monkshu_component.mjs";
import {loginform} from "../../js/loginform.mjs";
import { APP_CONSTANTS } from "../../js/constants.mjs";
import { apimanager as apiman } from "/framework/js/apimanager.mjs";

async function redirect(){
	setTimeout(() => {
		loginform.LoadProfilePage();
	}, 300);
}

function logout(){
	loginform.setLogoutButton();
}

async function loadImage(){
	let requestObject = {id:encryptSession.get(APP_CONSTANTS.USER_DATA).id};
	let responseObject = await apiman.rest(APP_CONSTANTS.IMAGE_DATA, "POST", requestObject, true);
	let userImage = crumb_nav.shadowRoot.getElementById("userImage");
	if(responseObject.result!="None" && responseObject.result){
		userImage.src = responseObject.result;
	}
}
function register() {
	// convert this all into a WebComponent so we can use it
	monkshu_component.register("crumb-nav", `${APP_CONSTANTS.APP_PATH}/components/crumb-nav/crumb-nav.html`, crumb_nav);
}

const trueWebComponentMode = true;	// making this false renders the component without using Shadow DOM

export const crumb_nav = {trueWebComponentMode, register, redirect, loadImage, logout}