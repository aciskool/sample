/* 
 * (C) 2019 TekMonks. All rights reserved.
 * License: MIT - see enclosed license.txt file.
 */
import {router} from "/framework/js/router.mjs";
import { encryptSession } from "../../js/lib/encryptSession.mjs";
import {monkshu_component} from "/framework/js/monkshu_component.mjs";
import {loginform} from "../../js/loginform.mjs";
import { apimanager as apiman } from "/framework/js/apimanager.mjs";

async function loadUserData(){
	let requestObject = {id:encryptSession.get(APP_CONSTANTS.USER_DATA).id};
	let responseObject = await apiman.rest(APP_CONSTANTS.IMAGE_DATA, "POST", requestObject, true);

	let userData = encryptSession.get(APP_CONSTANTS.USER_DATA);
	let userImage = image_slider.shadowRoot.getElementById("ProfileImage");
	let username = image_slider.shadowRoot.getElementById("user_name");
	let designation = image_slider.shadowRoot.getElementById("user_desig");


	if(responseObject.result!="None" && responseObject.result){
		userImage.src = responseObject.result;
	}

	username.innerHTML = userData.emp_name;
	designation.innerHTML = userData.Dept;
}

function register(){
	monkshu_component.register("image-slider", `${APP_CONSTANTS.APP_PATH}/components/image-slider/image-slider.html`, image_slider);
}

const trueWebComponentMode = true;	// making this false renders the component without using Shadow DOM

export const image_slider = {trueWebComponentMode, register, loadUserData}

