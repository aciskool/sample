import {i18n} from "/framework/js/i18n.mjs";
import {util} from "/framework/js/util.mjs";
import {router} from "/framework/js/router.mjs";
import {session} from "/framework/js/session.mjs";
import {monkshu_component} from "/framework/js/monkshu_component.mjs";
import { helpers } from "../../js/lib/helpers.mjs";
import { APP_CONSTANTS } from "../../js/constants.mjs";



function register() {
	// convert this all into a WebComponent so we can use it
	monkshu_component.register("multiple-select", `${APP_CONSTANTS.APP_PATH}/components/multipleSelect/multipleSelect.html`, multipleSelect);
	setTimeout(async function(){

		let responseObject = await (await fetch(`${APP_CONSTANTS.LEAD_DATA}`, { method: "GET" })).json();

		try {
			let list = document.querySelector("body > page-generator").shadowRoot.querySelector("#registerform").shadowRoot.querySelector("#leads").shadowRoot.querySelector("#LeadOptions");
			
			for(var i=0;i<responseObject.leadInfo.length;i++){
				var option = document.createElement("option");
				option.innerHTML = responseObject.leadInfo[i].emp_name;
				option.value = responseObject.leadInfo[i].id;
				list.append(option);	
			}
		} catch (err) {
			console.log(err);
			let list = document.querySelector("body > page-generator").shadowRoot.querySelector("#editDetail").shadowRoot.querySelector("#leads").shadowRoot.querySelector("#LeadOptions");

			for(var i=0;i<responseObject.leadInfo.length;i++){
				var option = document.createElement("option");
				option.innerHTML = responseObject.leadInfo[i].emp_name;
				option.value = responseObject.leadInfo[i].id;
				list.append(option);	
			}
		}
	}, 400);
}


const trueWebComponentMode = true;	// making this false renders the component without using Shadow DOM

export const multipleSelect = {trueWebComponentMode, register}