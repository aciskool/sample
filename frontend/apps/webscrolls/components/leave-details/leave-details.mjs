/* 
 * (C) 2019 TekMonks. All rights reserved.
 * License: MIT - see enclosed license.txt file.
 */
import {i18n} from "/framework/js/i18n.mjs";
import {util} from "/framework/js/util.mjs";
import {router} from "/framework/js/router.mjs";
import { encryptSession } from "../../js/lib/encryptSession.mjs";
import {monkshu_component} from "/framework/js/monkshu_component.mjs";
import {loginform} from "../../js/loginform.mjs";
import { APP_CONSTANTS } from "../../js/constants.mjs";
import { apimanager as apiman } from "/framework/js/apimanager.mjs";


function register() {
	// convert this all into a WebComponent so we can use it
	monkshu_component.register("leave-details", `${APP_CONSTANTS.APP_PATH}/components/leave-details/leave-details.html`, leave_details);
}

const trueWebComponentMode = true;	// making this false renders the component without using Shadow DOM

export const leave_details = {trueWebComponentMode, register}